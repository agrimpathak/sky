package edu.berkeley.sky.report;

import java.util.List;

/**
 * Interface for all Recorders, i.e. classes that record events
 *
 * @author Agrim Pathak
 */
public interface Recorder {

  /**
   * @return A list of ReportRecords the portfolio has been recording.
   */
  List<Record> flushRecords();

  /**
   * @return A string specifying the destination path of the report
   */
  String path();

  /**
   * Record the portfolio's state at the current simulation timestamp
   */
  void record();
}
