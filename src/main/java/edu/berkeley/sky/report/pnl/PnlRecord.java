package edu.berkeley.sky.report.pnl;

import static edu.berkeley.sky.util.DateUtil.DATE_TIME_FORMAT;

import java.util.HashMap;
import java.util.Map;

import org.joda.time.base.AbstractInstant;

import edu.berkeley.sky.analytics.BlackScholes;
import edu.berkeley.sky.analytics.BlackScholesImpliedVolatility;
import edu.berkeley.sky.analytics.PortfolioAnalytics;
import edu.berkeley.sky.analytics.RelativePriceDelta;
import edu.berkeley.sky.market.Option;
import edu.berkeley.sky.market.SecurityType;
import edu.berkeley.sky.position.Portfolio;
import edu.berkeley.sky.position.Position;
import edu.berkeley.sky.report.Record;

/**
 * A ReportRecord is a class that holds data of particular position at some point in time.
 *
 * @author Agrim Pathak
 */
public class PnlRecord implements Record {

  private static final BlackScholes bs = new BlackScholes();
  private static final BlackScholesImpliedVolatility bsiv = new BlackScholesImpliedVolatility();
  private final static String[] header;
  private final static String HEADER_ASK = "ask";
  private final static String HEADER_BID = "bid";
  private final static String HEADER_BS_FIXED_VOL_DELTA = "bs_fixed_vol_delta";
  private final static String HEADER_BS_IV_DELTA = "bs_iv_delta";
  private final static String HEADER_BS_IV_THETA = "bs_iv_theta";
  private final static String HEADER_BS_THETA = "bs_fixed_vol_theta";
  private final static String HEADER_BS_VEGA = "bs_fixed_vol_vega";
  private final static String HEADER_BS_IV_VEGA = "bs_iv_vega";
  private final static String HEADER_COMMISSION = "commission";
  private final static String HEADER_LAST_REFRESH = "last_refresh";
  private final static String HEADER_NEXT_REFRESH = "next_refresh";
  private final static String HEADER_PORT_REALIZED_PNL = "overall_realized_pnl";
  private final static String HEADER_PORT_TOTAL_PNL = "overall_total_pnl";
  private final static String HEADER_PORT_UNREALIZED_PNL = "overall_unrealized_pnl";
  private final static String HEADER_POS_REALIZED_PNL = "position_realized_pnl";
  private final static String HEADER_POS_TOTAL_PNL = "position_total_pnl";
  private final static String HEADER_POS_UNREALIZED_PNL = "position_unrealized_pnl";
  private final static String HEADER_POSITION_IV = "iv";
  private final static String HEADER_RELATIVE_DELTA = "relative_delta";
  private final static String HEADER_SECURITY = "security";
  private final static String HEADER_SECURITY_TYPE = "security_type";
  private final static String HEADER_SIZE = "size";
  private final static String HEADER_TIMESTAMP = "timestamp";
  private final static String HEADER_WA_PRICE = "wa_price";
  private static RelativePriceDelta relDelta;

  static {
    header = new String[] {
    /**/HEADER_TIMESTAMP,
    /**/HEADER_LAST_REFRESH,
    /**/HEADER_NEXT_REFRESH,
    /**/HEADER_SECURITY_TYPE,
    /**/HEADER_SECURITY,
    /**/HEADER_SIZE,
    /**/HEADER_BID,
    /**/HEADER_ASK,
    /**/HEADER_WA_PRICE,
    /**/HEADER_POSITION_IV,
    /**/HEADER_BS_FIXED_VOL_DELTA,
    /**/HEADER_BS_IV_DELTA,
    /**/HEADER_RELATIVE_DELTA,
    /**/HEADER_BS_VEGA,
    /**/HEADER_BS_IV_VEGA,
    /**/HEADER_BS_THETA,
    /**/HEADER_BS_IV_THETA,
    /**/HEADER_POS_UNREALIZED_PNL,
    /**/HEADER_POS_REALIZED_PNL,
    /**/HEADER_POS_TOTAL_PNL,
    /**/HEADER_PORT_UNREALIZED_PNL,
    /**/HEADER_PORT_REALIZED_PNL,
    /**/HEADER_PORT_TOTAL_PNL,
    /**/HEADER_COMMISSION};
  }

  static void setRelativeDelta(RelativePriceDelta relativeDelta) {
    relDelta = relativeDelta;
  }

  private final Map<String, Object> headerToValue = new HashMap<String, Object>();

  PnlRecord(AbstractInstant simDateTime, Portfolio portfolio, Position position) {
    headerToValue.put(HEADER_TIMESTAMP, simDateTime.toString(DATE_TIME_FORMAT));
    headerToValue.put(HEADER_LAST_REFRESH, position.security().minutesSinceTimestamp());
    headerToValue.put(HEADER_NEXT_REFRESH, position.security().minutesToNextTimestamp());
    headerToValue.put(HEADER_SECURITY_TYPE, position.security().type().toString());
    headerToValue.put(HEADER_SECURITY, position.security().ticker());
    headerToValue.put(HEADER_SIZE, position.size());
    try {
      headerToValue.put(HEADER_BID, position.security().bid());
      headerToValue.put(HEADER_ASK, position.security().ask());
    } catch (NullPointerException e) {
      headerToValue.put(HEADER_BID, "NA");
      headerToValue.put(HEADER_ASK, "NA");
    }
    headerToValue.put(HEADER_WA_PRICE, position.weightedAveragePrice());
    headerToValue.put(HEADER_BS_FIXED_VOL_DELTA, PortfolioAnalytics.delta(position, bs));
    headerToValue.put(HEADER_BS_IV_DELTA, PortfolioAnalytics.delta(position, bsiv));
    headerToValue.put(HEADER_RELATIVE_DELTA, PortfolioAnalytics.delta(position, relDelta));

    if (position.security().type() == SecurityType.Option) {
      int size = position.size();
      Option opt = (Option) position.security();
      headerToValue.put(HEADER_POSITION_IV, size * bsiv.impliedVolatility(opt));
      headerToValue.put(HEADER_BS_THETA, size * bs.theta(opt));
      headerToValue.put(HEADER_BS_IV_THETA, size * bsiv.theta(opt));
      
      headerToValue.put(HEADER_BS_VEGA, size * bs.vega(opt));
      headerToValue.put(HEADER_BS_IV_VEGA, size * bsiv.vega(opt));
      
    } else {
      headerToValue.put(HEADER_POSITION_IV, 0.0);
      headerToValue.put(HEADER_BS_THETA, 0.0);
      headerToValue.put(HEADER_BS_IV_THETA, 0.0);
      headerToValue.put(HEADER_BS_VEGA, 0.0);
      headerToValue.put(HEADER_BS_IV_VEGA, 0.0);
    }

    headerToValue.put(HEADER_POS_UNREALIZED_PNL, position.unrealizedPnl());
    headerToValue.put(HEADER_POS_REALIZED_PNL, position.realizedPnl());
    headerToValue.put(HEADER_POS_TOTAL_PNL, position.totalPnl());

    headerToValue.put(HEADER_PORT_UNREALIZED_PNL, portfolio.unrealizedPnl());
    headerToValue.put(HEADER_PORT_REALIZED_PNL, portfolio.realizedPnl());
    headerToValue.put(HEADER_PORT_TOTAL_PNL, portfolio.totalPnl());
    headerToValue.put(HEADER_COMMISSION, portfolio.commission());
  }

  @Override
  public Map<String, Object> contents() {
    return headerToValue;
  }

  @Override
  public String[] header() {
    return header;
  }
}
