package edu.berkeley.sky.report.pnl;

import org.joda.time.MutableDateTime;

import edu.berkeley.sky.analytics.RelativePriceDelta;
import edu.berkeley.sky.market.Market;
import edu.berkeley.sky.position.Portfolio;
import edu.berkeley.sky.position.Position;
import edu.berkeley.sky.report.AbstractRecorder;

/**
 * Class that records events
 *
 * @author Agrim Pathak
 */
public class PnlRecorder extends AbstractRecorder {

  private final Portfolio portfolio;

  public PnlRecorder(MutableDateTime simDateTime, String fileName, Market market,
      Portfolio portfolio) {
    super(simDateTime, fileName);
    this.portfolio = portfolio;
    PnlRecord.setRelativeDelta(new RelativePriceDelta(market));
  }

  @Override
  public void record() {
    for (Position position : portfolio.positions()) {
      reportRecords.add(new PnlRecord(simDateTime, portfolio, position));
    }
  }
}
