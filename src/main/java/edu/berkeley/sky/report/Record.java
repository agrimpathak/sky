package edu.berkeley.sky.report;

import java.util.Map;

/**
 * Interface for records used in reporting
 *
 * @author Agrim Pathak
 */
public interface Record {

  /**
   * @return The field to entry mapping.
   */
  Map<String, Object> contents();

  /**
   * @return The record's field names
   */
  String[] header();
}
