package edu.berkeley.sky.report.data.extractor;

import edu.berkeley.sky.market.Security;

/**
 * Interface for extracting a particular price of a security
 *
 * @author Agrim Pathak
 */
public interface DataExtractor {

  /**
   * @param security
   * @return Extract the security's price. This may be a raw price or a calculated price.
   */
  double extract(Security security);

}
