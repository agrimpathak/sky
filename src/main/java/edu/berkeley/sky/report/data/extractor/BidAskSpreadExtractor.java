package edu.berkeley.sky.report.data.extractor;

import edu.berkeley.sky.market.Security;

/**
 * Retrieves the bid-ask spread of a security
 *
 * @author Agrim Pathak
 */
public class BidAskSpreadExtractor extends AbstractDataExtractor {

  public BidAskSpreadExtractor(int maxLookback) {
    super(maxLookback);
  }

  @Override
  public double extract(Security security) {
    if (isStale(security)) {
      return 0.0;
    }
    return security.bidAskSpread();
  }
}
