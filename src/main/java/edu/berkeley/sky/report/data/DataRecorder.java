package edu.berkeley.sky.report.data;

import org.joda.time.MutableDateTime;

import edu.berkeley.sky.market.Market;
import edu.berkeley.sky.report.AbstractRecorder;
import edu.berkeley.sky.report.data.extractor.DataExtractor;

/**
 * A price recorder records the price of a security according to PriceExtractor parameter
 *
 * @author agrimpathak
 * @param <P> The type of price that should be extracted
 */
public class DataRecorder<P extends DataExtractor> extends AbstractRecorder {

  private final P extractor;
  private final Market market;
  private final int minuteOfHour;

  public DataRecorder(MutableDateTime simDateTime, String fileName, Market market, P extractor,
      int minuteOfHour) {
    super(simDateTime, fileName);
    this.market = market;
    this.minuteOfHour = minuteOfHour;
    this.extractor = extractor;
  }

  @Override
  public void record() {
    if ((simDateTime.getMinuteOfHour() % minuteOfHour) == 0) {
      reportRecords.add(new DataRecord<P>(simDateTime, market, extractor, minuteOfHour));
    }
  }
}
