package edu.berkeley.sky.report.data;

import static edu.berkeley.sky.util.DateUtil.DATE_TIME_FORMAT;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.joda.time.base.AbstractInstant;

import edu.berkeley.sky.market.Market;
import edu.berkeley.sky.market.Option;
import edu.berkeley.sky.market.OptionType;
import edu.berkeley.sky.market.Security;
import edu.berkeley.sky.market.SecurityType;
import edu.berkeley.sky.report.Record;
import edu.berkeley.sky.report.data.extractor.DataExtractor;

/**
 * Abstract class for PriceRecords. Prices can be represented in several ways, e.g. raw prices,
 * implied volatility, Monte Carlo etc
 *
 * @author Agrim Pathak
 */
public class DataRecord<P extends DataExtractor> implements Record {

  private static String[] header;
  private final static String HEADER_TIMESTAMP = "Timestamp";

  private static final Comparator<Security> headerComp = new Comparator<Security>() {
    @Override
    public int compare(Security s1, Security s2) {
      if (s1.type() == s2.type()) {
        if (s1.type() == SecurityType.Option) {
          Option o1 = (Option) s1;
          Option o2 = (Option) s2;
          if (o1.optionType() == o2.optionType()) {
            return o1.strike() < o2.strike() ? -1 : 1;
          } else {
            return o1.optionType() == OptionType.CALL ? -1 : 1;
          }
        }
        return s1.ticker().compareTo(s2.ticker());
      }
      return SecurityType.securityTypeOrdering.get(s1.type())
      /* */- SecurityType.securityTypeOrdering.get(s2.type());
    }
  };

  public static void initHeader(Market market) {
    List<Security> securities = new ArrayList<Security>(market.watchlist().size());
    for (Security sec : market) {
      securities.add(sec);
    }
    Collections.sort(securities, headerComp);
    header = new String[1 + securities.size()];
    header[0] = HEADER_TIMESTAMP;
    int i = 1;
    for (Security sec : securities) {
      header[i++] = sec.ticker();
    }
  }

  private final Map<String, Object> headerToValue = new HashMap<String, Object>();

  DataRecord(AbstractInstant simDateTime, Market market, P extractor, int minuteOfHour) {
    headerToValue.put(HEADER_TIMESTAMP, simDateTime.toString(DATE_TIME_FORMAT));
    for (Security sec : market) {
      headerToValue.put(sec.ticker(), extractor.extract(sec));
    }
  }

  @Override
  public Map<String, Object> contents() {
    return headerToValue;
  }

  @Override
  public String[] header() {
    return header;
  }
}
