package edu.berkeley.sky.report.data.extractor;

import edu.berkeley.sky.market.Security;

/**
 * @author Agrim Pathak
 */
abstract class AbstractDataExtractor implements DataExtractor {

  private final int maxLookback;

  AbstractDataExtractor(int maxLookback) {
    this.maxLookback = maxLookback;
  }

  /**
   * @param security
   * @return A boolean indicating whether the security's data is stale, i.e. not usable
   */
  protected boolean isStale(Security security) {
    return !security.hasPriceData() || ((security.minutesSinceTimestamp() > maxLookback) ||
    /**/(security.bid() <= 0) || (security.ask() <= 0) || (security.bid() > security.ask()));
  }
}
