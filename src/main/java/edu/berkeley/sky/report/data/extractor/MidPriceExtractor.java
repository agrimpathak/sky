package edu.berkeley.sky.report.data.extractor;

import edu.berkeley.sky.market.Security;

/**
 * Extracts a security's mid price
 *
 * @author Agrim Pathak
 */
public class MidPriceExtractor extends AbstractDataExtractor {

  public MidPriceExtractor(int maxLookback) {
    super(maxLookback);
  }

  @Override
  public double extract(Security security) {
    if (isStale(security)) {
      return 0.0;
    }
    return security.mid();
  }
}
