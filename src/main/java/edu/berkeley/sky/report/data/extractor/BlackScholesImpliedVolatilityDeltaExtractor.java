package edu.berkeley.sky.report.data.extractor;

import edu.berkeley.sky.analytics.BlackScholesImpliedVolatility;
import edu.berkeley.sky.market.Option;
import edu.berkeley.sky.market.Security;
import edu.berkeley.sky.market.SecurityType;

/**
 * Extracts the BS delta of an option using the BS-IV as volatility
 *
 * @author Agrim Pathak
 */
public class BlackScholesImpliedVolatilityDeltaExtractor extends AbstractDataExtractor {

  private final BlackScholesImpliedVolatility bsiv = new BlackScholesImpliedVolatility();

  public BlackScholesImpliedVolatilityDeltaExtractor(int maxLookback) {
    super(maxLookback);
  }

  @Override
  public double extract(Security security) {
    if (isStale(security)) {
      return 0.0;
    }
    if (security.type() != SecurityType.Option) {
      return security.mid();
    }
    return bsiv.delta((Option) security);
  }
}
