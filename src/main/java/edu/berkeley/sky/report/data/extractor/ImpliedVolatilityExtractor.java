package edu.berkeley.sky.report.data.extractor;

import edu.berkeley.sky.analytics.BlackScholesImpliedVolatility;
import edu.berkeley.sky.market.Option;
import edu.berkeley.sky.market.Security;
import edu.berkeley.sky.market.SecurityType;

/**
 * If the security is an option, returns its implied volatiliiy, otherwise returns its mid price.
 *
 * @author Agrim Pathak
 */
public class ImpliedVolatilityExtractor extends AbstractDataExtractor {

  private final BlackScholesImpliedVolatility bsiv = new BlackScholesImpliedVolatility();

  public ImpliedVolatilityExtractor(int maxLookback) {
    super(maxLookback);
  }

  @Override
  public double extract(Security security) {
    if (isStale(security)) {
      return 0.0;
    }
    if (security.type() != SecurityType.Option) {
      return security.mid();
    }
    return bsiv.impliedVolatility((Option) security);
  }
}
