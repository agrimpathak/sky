package edu.berkeley.sky.report.data.extractor;

import edu.berkeley.sky.analytics.RelativePriceDelta;
import edu.berkeley.sky.market.Market;
import edu.berkeley.sky.market.Option;
import edu.berkeley.sky.market.Security;
import edu.berkeley.sky.market.SecurityType;

/**
 * Retrieves the relative-price delta
 *
 * @author Agrim Pathak
 */
public class RelativePriceDeltaExtractor extends AbstractDataExtractor {

  private final RelativePriceDelta rpd;

  public RelativePriceDeltaExtractor(int maxLookback, Market market) {
    super(maxLookback);
    rpd = new RelativePriceDelta(market);
  }

  @Override
  public double extract(Security security) {
    if (isStale(security)) {
      return 0.0;
    }
    if (security.type() != SecurityType.Option) {
      return security.mid();
    }
    return rpd.delta((Option) security);
  }
}
