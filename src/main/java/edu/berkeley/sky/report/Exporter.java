package edu.berkeley.sky.report;

import static edu.berkeley.sky.util.Preconditions.checkArgument;
import static edu.berkeley.sky.util.Preconditions.checkNotNull;

import java.io.FileWriter;
import java.io.IOException;
import java.util.Iterator;
import java.util.List;

import org.supercsv.io.CsvMapWriter;
import org.supercsv.io.ICsvMapWriter;
import org.supercsv.prefs.CsvPreference;

/**
 * A class that writes List<ReportRecord> to a csv file.
 *
 * @author Agrim Pathak
 */
public class Exporter {

  /**
   * Write the records to a csv file.
   *
   * @param records
   * @throws IOException
   */
  public static void writeCsv(List<Record> records, String path) {
    checkNotNull(records != null);
    checkArgument(records.size() > 0);
    /*
     * Extract the header from the first record
     */
    Iterator<Record> iter = records.iterator();
    Record next = iter.next();
    final String[] header = next.header();
    /*
     * Initialize the MapWriter
     */
    ICsvMapWriter mapWriter = null;
    try {
      mapWriter = new CsvMapWriter(new FileWriter(path), CsvPreference.STANDARD_PREFERENCE);
      /*
       * Write the header, and the first record
       */
      mapWriter.writeHeader(header);
      mapWriter.write(next.contents(), header);
      next = null;
      /*
       * Write the rest of the records
       */
      while (iter.hasNext()) {
        mapWriter.write(iter.next().contents(), header);
      }
      if (mapWriter != null) {
        mapWriter.close();
      }
    } catch (IOException e) {
      e.printStackTrace();
      System.exit(1);
    }
    System.out.println("Exported to " + path);
  }
}
