package edu.berkeley.sky.report;

import java.io.File;
import java.util.LinkedList;
import java.util.List;

import org.joda.time.MutableDateTime;

/**
 * Abstract class that records events
 *
 * @author Agrim Pathak
 */
public abstract class AbstractRecorder implements Recorder {

  private final String filePath;
  protected final List<Record> reportRecords = new LinkedList<Record>();
  protected final MutableDateTime simDateTime;

  protected AbstractRecorder(MutableDateTime simDateTime, String fileName) {
    this.simDateTime = simDateTime;
    filePath = new StringBuilder()
    /**/.append(System.getProperty("user.dir"))
    /**/.append(File.separator)
    /**/.append("export")
    /**/.append(File.separator)
    /**/.append(fileName)
    /**/.toString();
  }

  @Override
  public List<Record> flushRecords() {
    return reportRecords;
  }

  @Override
  public String path() {
    return filePath;
  }
}
