package edu.berkeley.sky.database;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.HashMap;
import java.util.Map;

import org.joda.time.LocalDate;
import org.joda.time.base.AbstractInstant;

import edu.berkeley.sky.market.Security;
import edu.berkeley.sky.market.SecurityType;

/**
 * Class containing methods that create string queries.
 *
 * @author Agrim Pathak
 */
public class Queries {

  private static final String FIELD_EXPIRATION = "expiration";
  private static final String FIELD_SECURITY = "security";
  private static final String FIELD_TIMESTAMP = "price_timestamp";
  private static final String FIELD_UNDERLYING = "underlying";
  private static final Map<SecurityType, String> securityTypeToTable =
  /**/new HashMap<SecurityType, String>();
  private static final String TABLE_FUTURES = "futures";
  private static final String TABLE_INDEX = "index";
  private static final String TABLE_OPTION = "option";
  private static final String TABLE_OPTION_EXPIRATIONS = "option_expirations";
  private static final String TABLE_STOCK = "stock";

  static {
    securityTypeToTable.put(SecurityType.Futures, TABLE_FUTURES);
    securityTypeToTable.put(SecurityType.Index, TABLE_INDEX);
    securityTypeToTable.put(SecurityType.Option, TABLE_OPTION);
    securityTypeToTable.put(SecurityType.Stock, TABLE_STOCK);
  }

  private final Map<SecurityType, PreparedStatement> securityTypeToPreparedStatement =
  /**/new HashMap<SecurityType, PreparedStatement>();

  public Queries(Dao dao) {
    for (Map.Entry<SecurityType, String> entry : securityTypeToTable.entrySet()) {
      securityTypeToPreparedStatement.put(
      /**/entry.getKey(), dao.prepare(preparedStatementQuery(entry.getValue())));
    }
  }

  /**
   * @param startDateTime
   * @return A SQL query of all available option and expirations available in the database after the
   *         supplied startDate.
   */
  public String availableOptionsQuery(LocalDate startDate) {
    return new StringBuilder()
    /**/.append("SELECT ")
    /**/.append(FIELD_UNDERLYING)
    /**/.append(", ")
    /**/.append(FIELD_EXPIRATION)
    /**/.append(" FROM ")
    /**/.append(TABLE_OPTION_EXPIRATIONS)
    /**/.append(" WHERE ")
    /**/.append("'")
    /**/.append(startDate.toString())
    /**/.append("'")
    /**/.append(" <= ")
    /**/.append(FIELD_EXPIRATION)
    /**/.append(" ORDER BY ")
    /**/.append(FIELD_UNDERLYING)
    /**/.append(", ")
    /**/.append(FIELD_EXPIRATION)
    /**/.append(" ASC")
    /**/.toString();
  }

  /**
   * @param table
   * @return A SQL query to be used as a prepared statement. The query returns price data of a
   *         security.
   */
  private String preparedStatementQuery(String table) {
    return new StringBuilder()
    /**/.append("SELECT * FROM ")
    /**/.append(table)
    /**/.append(" WHERE ")
    /**/.append(FIELD_SECURITY)
    /**/.append(" = ")
    /**/.append("?")
    /**/.append(" AND ")
    /**/.append(FIELD_TIMESTAMP)
    /**/.append(" BETWEEN ")
    /**/.append("?")
    /**/.append(" AND ?")
    /**/.append(" ORDER BY ")
    /**/.append(FIELD_TIMESTAMP)
    /**/.append(" ASC")
    /**/.toString();
  }

  /**
   * @param security
   * @param fromDateTime
   * @param endDateTime
   * @return A ResultSet of price data of supplied security during the supplied time interval.
   */
  public ResultSet priceData(Security security, AbstractInstant fromDateTime,
      AbstractInstant endDateTime) {
    PreparedStatement preparedStmt = securityTypeToPreparedStatement.get(security.type());
    ResultSet result = null;
    try {
      preparedStmt.setString(1, security.ticker());
      preparedStmt.setTimestamp(2, new Timestamp(fromDateTime.getMillis()));
      preparedStmt.setTimestamp(3, new Timestamp(endDateTime.getMillis()));
      result = preparedStmt.executeQuery();
    } catch (SQLException e) {
      e.printStackTrace();
      System.exit(1);
    }
    return result;
  }
}
