package edu.berkeley.sky.database;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

/**
 * Dao (Database access object) class. Stores database properties such as url, user name and
 * password. Enables the client to connect, disconnect and execute queries against the database.
 *
 * @author Agrim Pathak
 */
public class Dao {

  private static final String DEFAULT_PASSWORD = "postgres";
  private static final String DEFAULT_URL = "jdbc:postgresql://localhost:5432/bloomberg";
  private static final String DEFAULT_USER = "postgres";

  public static Dao defaultDao() {
    return new Dao(DEFAULT_URL, DEFAULT_USER, DEFAULT_PASSWORD);
  }

  private Connection con;
  private final String password;
  private final String url;
  private final String user;

  public Dao(String url, String user, String password) {
    this.url = url;
    this.user = user;
    this.password = password;
  }

  /**
   * Connect to the database.
   */
  public void connect() {
    try {
      con = DriverManager.getConnection(url, user, password);
    } catch (SQLException e) {
      e.printStackTrace();
      System.exit(1);
    }
  }

  /**
   * Disconnect from the database.
   */
  public void disconnect() {
    if (con != null) {
      try {
        con.close();
      } catch (SQLException e) {
        e.printStackTrace();
        System.exit(1);
      }
    }
  }

  /**
   * Run a SQL query against the database and return a ResultSet.
   *
   * @param query Query to execute
   * @return A result set of the query.
   */
  public ResultSet executeQuery(String query) {
    try {
      return con.createStatement().executeQuery(query);
    } catch (SQLException e) {
      disconnect();
      e.printStackTrace();
      System.exit(1);
      return null;
    }
  }

  /**
   * @return A boolean indicating that the Dao is conneted to the database.
   */
  public boolean isConnected() {
    try {
      return con == null ? false : !con.isClosed();
    } catch (SQLException e) {
      e.printStackTrace();
      disconnect();
      System.exit(1);
      return false;
    }
  }

  /**
   * @param query
   * @return A prepared statement
   */
  public PreparedStatement prepare(String query) {
    try {
      return con.prepareStatement(query);
    } catch (SQLException e) {
      e.printStackTrace();
      System.exit(1);
      return null;
    }
  }
}
