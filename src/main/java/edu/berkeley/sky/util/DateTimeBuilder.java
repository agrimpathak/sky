package edu.berkeley.sky.util;

import static edu.berkeley.sky.util.Preconditions.checkArgument;

import org.joda.time.DateTime;
import org.joda.time.MutableDateTime;

/**
 * DateTime and MutableDateTime builders
 *
 * @author Agrim Pathak
 */
public class DateTimeBuilder {

  private int day;
  private int hour;
  private int millisecond;
  private int minute;
  private int month;
  private int second;
  private int year;

  DateTimeBuilder() {}

  public DateTime build() {
    validate();
    return new DateTime(year, month, day, hour, minute, second, millisecond);
  }

  public MutableDateTime buildMutable() {
    validate();
    return new MutableDateTime(year, month, day, hour, minute, second, millisecond);
  }

  private void validate() {
    checkArgument(year > 0, month > 0, day > 0, hour >= 0, minute >= 0, second >= 0,
        millisecond >= 0);
  }

  public DateTimeBuilder withDay(int day) {
    this.day = day;
    return this;
  }

  public DateTimeBuilder withHour(int hour) {
    this.hour = hour;
    return this;
  }

  public DateTimeBuilder withMillisecond(int millisecond) {
    this.millisecond = millisecond;
    return this;
  }

  public DateTimeBuilder withMinute(int minute) {
    this.minute = minute;
    return this;
  }

  public DateTimeBuilder withMonth(int month) {
    this.month = month;
    return this;
  }

  public DateTimeBuilder withSecond(int second) {
    this.second = second;
    return this;
  }

  public DateTimeBuilder withYear(int year) {
    this.year = year;
    return this;
  }
}
