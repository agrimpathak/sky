package edu.berkeley.sky.util;

/**
 * Utility class containing constants.
 * 
 * @author Agrim Pathak
 */
public class Constants {
  public final static int DAYS_PER_YEAR = 365;
  public final static int HOURS_PER_DAY = 24;
  public final static int MILLIS_PER_SECOND = 1000;
  public final static int MINUTES_PER_DAY = 24 * 60;
  public final static int MINUTES_PER_HOUR = 60;
  public final static double RISK_FREE_RATE = 0.01;
  public final static int SECONDS_PER_MINUTE = 60;
}
