package edu.berkeley.sky.util;

import edu.berkeley.sky.market.Security;

/**
 * @author Agrim Pathak
 */
public class OptionUtil {

  public static int optionPriceMultiple = 1; // TODO hack

  public static int atTheMoneyPrice(Security underlying) {
    double mid = underlying.mid();
    int roundDown = ((int) (mid / optionPriceMultiple)) * optionPriceMultiple;
    int roundUp = roundDown + optionPriceMultiple;
    return (mid - roundDown) < (roundUp - mid) ? roundDown : roundUp;
  }
}
