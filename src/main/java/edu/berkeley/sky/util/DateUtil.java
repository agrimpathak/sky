package edu.berkeley.sky.util;

import org.joda.time.DateTimeConstants;
import org.joda.time.LocalDate;
import org.joda.time.LocalTime;
import org.joda.time.ReadableDateTime;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;

import static edu.berkeley.sky.util.Constants.HOURS_PER_DAY;
import static edu.berkeley.sky.util.Constants.MILLIS_PER_SECOND;
import static edu.berkeley.sky.util.Constants.MINUTES_PER_HOUR;
import static edu.berkeley.sky.util.Constants.SECONDS_PER_MINUTE;

/**
 * Utility class containing useful date methods.
 *
 * @author Agrim Pathak
 */
public class DateUtil {

  public static final LocalTime _MARKET_CLOSE_TIME = new LocalTime(13, 00);
  public static final LocalTime _MARKET_OPEN_TIME = new LocalTime(6, 30);
  public static final TimeInterval ALL_HOURS =
  /**/new TimeInterval(new LocalTime(00, 00), new LocalTime(23, 59));
  public static final DateTimeFormatter DATE_TIME_FORMAT = DateTimeFormat
  /**/.forPattern("yyyy-MM-dd HH:mm:ss");
  public static final DateTimeFormatter LOCAL_DATE_FORMAT = DateTimeFormat
  /**/.forPattern("yyyy-MM-dd");
  public static final TimeInterval MARKET_HOURS =
  /**/new TimeInterval(_MARKET_OPEN_TIME, _MARKET_CLOSE_TIME);
  public final static int MINUTES_PER_YEAR = 52 * DateTimeConstants.MINUTES_PER_WEEK;

  /**
   * @return A builder to a DateTime or MutableDateTime
   */
  public static DateTimeBuilder dateTimeBuilder() {
    return new DateTimeBuilder();
  }

  /**
   * @param time
   * @return A boolean indicating whether the input falls on a weekend
   */
  public static boolean isWeekend(ReadableDateTime time) {
    int dow = time.getDayOfWeek();
    return (dow == DateTimeConstants.SATURDAY) || (dow == DateTimeConstants.SUNDAY);
  }

  /**
   * @param minutes Number of minutes to truncate off of the open and close of market hours
   * @return A time interval of market hours such that the first and last minutes are truncated
   */
  public static TimeInterval marketHoursTruncated(int minutes) {
    return new TimeInterval(_MARKET_OPEN_TIME.plusMinutes(minutes),
    /*                    */_MARKET_CLOSE_TIME.minusMinutes(minutes));
  }

  /**
   * @param millis
   * @return The unrounded number of days a number of milliseconds covers.
   */
  public static double millisToDays(long millis) {
    return ((double) millis / (double) MILLIS_PER_SECOND)
    /**// (SECONDS_PER_MINUTE)
    /**// (MINUTES_PER_HOUR)
    /**// (HOURS_PER_DAY);
  }

  /**
   * @param dateTimeStr String in the format of "yyyy-MM-dd"
   * @return
   */
  public static LocalDate stringToLocalDate(String dateStr) {
    return LOCAL_DATE_FORMAT.parseLocalDate(dateStr);
  }
}
