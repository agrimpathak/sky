package edu.berkeley.sky.util;

import static java.lang.Math.pow;

/**
 * Utility class containing mathematical methods.
 * 
 * @author Agrim Pathak
 */
public class MathUtil {

  /**
   * Take an average of a series of doubles.
   * 
   * @param ds
   * @return
   */
  public static double average(double... ds) {
    double sum = 0;
    for (Double d : ds) {
      sum += d;
    }
    return sum / ds.length;
  }

  /**
   * Square a double.
   * 
   * @param arg
   * @return
   */
  public static double square(double arg) {
    return pow(arg, 2);
  }
}
