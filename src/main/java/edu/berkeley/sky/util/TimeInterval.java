package edu.berkeley.sky.util;

import static edu.berkeley.sky.util.Preconditions.checkArgument;
import static edu.berkeley.sky.util.Preconditions.checkNotNull;

import org.joda.time.DateTime;
import org.joda.time.LocalTime;
import org.joda.time.MutableDateTime;

/**
 * A time interval. Supports intervals that overlap midnight.
 *
 * @author Agrim Pathak
 */
public class TimeInterval {

  private final boolean overlapsMidnight;
  private final LocalTime t1;
  private final LocalTime t2;

  public TimeInterval(LocalTime t1, LocalTime t2) {
    checkNotNull(t1, t2);
    checkArgument(!t1.equals(t2));
    overlapsMidnight = !t1.isBefore(t2);
    this.t1 = t1;
    this.t2 = t2;
  }

  public TimeInterval(TimeInterval ti) {
    checkNotNull(ti);
    overlapsMidnight = ti.overlapsMidnight;
    t1 = new LocalTime(ti.t1);
    t2 = new LocalTime(ti.t2);
  }

  public boolean contains(DateTime dt) {
    return contains(dt.toLocalTime());
  }

  public boolean contains(LocalTime t) {
    return overlapsMidnight ?
    /**/!(t.isBefore(t1) && t.isAfter(t2)) :
    /**/!(t1.isAfter(t) || t.isAfter(t2));
  }

  public boolean contains(MutableDateTime dt) {
    return contains(dt.toDateTime().toLocalTime());
  }

  public LocalTime endTime() {
    return t2;
  }

  public LocalTime startTime() {
    return t1;
  }

  public boolean strictlyContains(DateTime dt) {
    return strictlyContains(dt.toLocalTime());
  }

  public boolean strictlyContains(LocalTime t) {
    return overlapsMidnight ?
    /**/t1.isBefore(t) || t.isBefore(t2) :
    /**/t1.isBefore(t) && t.isBefore(t2);
  }

  public boolean strictlyContains(MutableDateTime dt) {
    return strictlyContains(dt.toDateTime().toLocalTime());
  }
}
