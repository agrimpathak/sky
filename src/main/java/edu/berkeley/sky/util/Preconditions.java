package edu.berkeley.sky.util;

/**
 * Utility class containing methods for checking conditions.
 * 
 * @author Agrim Pathak
 */
public class Preconditions {

  /**
   * Ensure the input boolean is true, otherwise throw IllegalArgumentException.
   * 
   * @param b
   */
  public static void checkArgument(boolean b) {
    if (!b) {
      throw new IllegalArgumentException();
    }
  }

  /**
   * Ensure the input boolean is true, otherwise throw IllegalArgumentException.
   * 
   * @param b
   */
  public static void checkArgument(boolean... b) {
    for (boolean _b : b) {
      if (!_b) {
        throw new IllegalArgumentException();
      }
    }
  }

  /**
   * Ensure the input boolean is true, otherwise throw IllegalArgumentException.
   * 
   * @param b
   */
  public static void checkArgument(boolean b, String msg) {
    if (!b) {
      throw new IllegalArgumentException(msg);
    }
  }

  /**
   * Ensure the input is not null, otherwise throw IllegalArgumentException.
   * 
   * @param b
   */
  public static void checkNotNull(Object obj) {
    if (obj == null) {
      throw new IllegalArgumentException();
    }
  }

  /**
   * Ensure the input is not null, otherwise throw IllegalArgumentException.
   * 
   * @param b
   */
  public static void checkNotNull(Object... objects) {
    for (Object obj : objects) {
      checkNotNull(obj);
    }
  }

  /**
   * Ensure the input is not null, otherwise throw IllegalArgumentException.
   * 
   * @param b
   */
  public static void checkNotNull(Object obj, String msg) {
    if (obj == null) {
      throw new IllegalArgumentException(msg);
    }
  }

  /**
   * Ensure the input is not null, otherwise throw IllegalArgumentException.
   * 
   * @param b
   */
  public static void checkNotNull(String msg, Object... objects) {
    for (Object obj : objects) {
      checkNotNull(obj, msg);
    }
  }
}
