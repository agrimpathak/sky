package edu.berkeley.sky.util;

/**
 * Utility class containing string manipulation methods.
 * 
 * @author Agrim Pathak
 */
public class StringUtil {

  /**
   * Filter out a string from another string.
   * 
   * @param arg Input string
   * @param filter String to remove
   * @return The input string with the filter string removed
   */
  public static String filterOut(String arg, String filter) {
    String[] parts = arg.split("\\s+");
    StringBuilder sb = new StringBuilder();
    for (String part : parts) {
      if (part.equals(filter)) {
        continue;
      }
      sb.append(part);
      sb.append(" ");
    }
    return sb.delete(-1 + sb.length(), sb.length()).toString();
  }

}
