package edu.berkeley.sky.market;

import org.joda.time.MutableDateTime;

/**
 * Stock security class.
 * 
 * @author Agrim Pathak
 */
public class Stock extends AbstractSecurity {

  Stock(String ticker, MutableDateTime simDateTime) {
    super(ticker, simDateTime);
  }

  @Override
  public SecurityType type() {
    return SecurityType.Stock;
  }
}
