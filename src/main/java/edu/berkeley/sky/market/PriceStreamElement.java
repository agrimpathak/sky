package edu.berkeley.sky.market;

import org.joda.time.DateTime;

/**
 * A cluster of bid, ask and trade price bars at a specific time.
 *
 * @author Agrim Pathak
 */
public class PriceStreamElement {

  private final double ask;
  private final double bid;
  private final double close;
  private final double high;
  private final double low;
  private final double open;
  private final DateTime timestamp;

  PriceStreamElement(DateTime timestamp, double open, double high, double low, double close,
      double bid, double ask) {
    this.timestamp = timestamp;
    this.open = open;
    this.high = high;
    this.low = low;
    this.close = close;
    this.bid = bid;
    this.ask = ask;
  }

  /**
   * @return Return the ask price
   */
  double ask() {
    return ask;
  }

  /**
   * @return Return the bid price
   */
  double bid() {
    return bid;
  }

  /**
   * @return Return the last trade price in the time interval
   */
  double close() {
    return close;
  }

  /**
   * @return Return the highest trade price in the time time interval
   */
  double high() {
    return high;
  }

  /**
   * @return Return the lowest trade price in the time interval
   */
  double low() {
    return low;
  }

  /**
   * @return Return the first trade price in the time interval
   */
  double open() {
    return open;
  }

  /**
   * @return The DateTime at which the prices are reflective of.
   */
  DateTime timestamp() {
    return timestamp;
  }
}
