package edu.berkeley.sky.market;

import static edu.berkeley.sky.util.Constants.DAYS_PER_YEAR;
import static edu.berkeley.sky.util.DateUtil.millisToDays;
import static edu.berkeley.sky.util.StringUtil.filterOut;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.joda.time.DateTime;
import org.joda.time.LocalDate;
import org.joda.time.MutableDateTime;
import org.joda.time.format.DateTimeFormat;

/**
 * Option security class.
 *
 * @author Agrim Pathak
 */
public class Option extends AbstractDerivative {

  /**
   * Create and return an option's Bloomberg ticker, e.g. "SPY US 05/09/2014 C170.0 EQUITY"
   *
   * @param underlying
   * @param expiration
   * @param strike
   * @param optionType
   * @return
   */
  static String optionAttributeToBloombergId(String underlying, LocalDate expiration,
      double strike, OptionType optionType) {
    return new StringBuilder()
    /**/.append(filterOut(underlying, "EQUITY"))
    /**/.append(" ")
    /**/.append(expiration.toString(DateTimeFormat.forPattern("MM/dd/yyyy")))
    /**/.append(" ")
    /**/.append(optionType.getName())
    /**/.append(strike)
    /**/.append(" EQUITY")
    /**/.toString();
  }

  private final LocalDate expiration;

  private final OptionType optionType;
  private final double strike;

  Option(MutableDateTime simDateTime, Security underlying, LocalDate expiration, double strike,
      OptionType optionType) {
    super(optionAttributeToBloombergId(underlying.ticker(), expiration, strike, optionType),
    /**/simDateTime, 100, underlying);
    this.expiration = expiration;
    this.strike = strike;
    this.optionType = optionType;
  }

  /**
   * @return The time to expiration in days unrounded.
   */
  public double daysToExpiration() {
    return millisToDays(millisToExpiration());
  }

  /**
   * The option expiration date.
   */
  public LocalDate expiration() {
    return expiration;
  }

  @Override
  void fillPrice(ResultSet rs) {
    priceStream.clear();
    DateTime dateTime;
    try {
      while (rs.next()) {
        dateTime = new DateTime(rs.getTimestamp("price_timestamp"));
        double bid = rs.getDouble("close_bid");
        double ask = rs.getDouble("close_ask");
        double close = rs.getDouble("close");
        if (priceDataIsValid(bid, ask)) {
          priceStream.add(new PriceStreamElement(dateTime, close, close, close, close, bid, ask));
        }
      }
    } catch (SQLException e) {
      e.printStackTrace();
      System.exit(1);
    }
    refresh();
  }

  /**
   * @return A boolean indicating whether the option is expired
   */
  // TODO: Inaccurate for options expiring on Saturday
  boolean isExpired() {
    return millisToExpiration() <= 0L;
  }

  /**
   * @return The time to expiration in milliseconds
   */
  public long millisToExpiration() {
    return expiration.toDateTimeAtStartOfDay().plusDays(1).getMillis() - simDateTime.getMillis();
  }

  /**
   * @return The option's OptionType, e.g. call or put.
   */
  public OptionType optionType() {
    return optionType;
  }

  /**
   * @param bid
   * @param ask
   * @return A boolean indicating whether prices are valid
   */
  private boolean priceDataIsValid(double bid, double ask) {
    return (bid > 0) && (ask > 0) && (bid < ask);
  }

  /**
   * @return The option's strike price.
   */
  public double strike() {
    return strike;
  }

  @Override
  public SecurityType type() {
    return SecurityType.Option;
  }

  /**
   * @return The time to expiration in years unrounded.
   */
  public double yearsToExpiration() {
    return daysToExpiration() / DAYS_PER_YEAR;
  }
}
