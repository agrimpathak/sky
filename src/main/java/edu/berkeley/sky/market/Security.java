package edu.berkeley.sky.market;

import org.joda.time.DateTime;

/**
 * A Security interface.
 *
 * @author Agrim Pathak
 */
public interface Security {

  /**
   * @return The current bar representing ask prices.
   */
  double ask();

  /**
   * @return The current bar representing bid prices.
   */
  double bid();

  /**
   * @return The bid-ask spread, i.e. the ask price minus the bid price
   */
  double bidAskSpread();

  /**
   * @return The last trade price in the time interval.
   */
  double close();

  /**
   * @return A boolean indicating that price data exists on or before the current simulation time.
   */
  boolean hasPriceData();

  /**
   * @return The highest trade price in the time interval.
   */
  double high();

  /**
   * @return The lowest trade price in the time interval.
   */
  double low();

  /**
   * @return The average of the bid and ask price.
   */
  double mid();

  /**
   * @return Minutes since the latest timestamp of available price data.
   */
  int minutesSinceTimestamp();

  /**
   * @return Minutes to next timestamp with available price data.
   */
  int minutesToNextTimestamp();

  /**
   * @return The first trade price in the time interval.
   */
  double open();

  /**
   * @return The security's ticker.
   */
  String ticker();

  /**
   * @return The DateTime representing the last time prices were refreshed.
   */
  DateTime timestamp();

  /**
   * @return The security's type, e.g. stock, option.
   */
  SecurityType type();
}
