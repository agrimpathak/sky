package edu.berkeley.sky.market;

import org.joda.time.MutableDateTime;

public class Futures extends AbstractDerivative {

  Futures(String ticker, MutableDateTime simDateTime, int contractSize, Security underlying) {
    super(ticker, simDateTime, contractSize, underlying);
  }

  @Override
  public SecurityType type() {
    return SecurityType.Futures;
  }
}
