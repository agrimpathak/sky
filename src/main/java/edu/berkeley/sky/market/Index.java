package edu.berkeley.sky.market;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.joda.time.DateTime;
import org.joda.time.MutableDateTime;

public class Index extends AbstractSecurity {

  Index(String ticker, MutableDateTime simDateTime) {
    super(ticker, simDateTime);
  }

  @Override
  void fillPrice(ResultSet rs) {
    priceStream.clear();
    DateTime dateTime;
    try {
      while (rs.next()) {
        dateTime = new DateTime(rs.getTimestamp("price_timestamp"));
        double open = rs.getDouble("open");
        double high = rs.getDouble("high");
        double low = rs.getDouble("low");
        double close = rs.getDouble("close");
        if (priceDataIsValid(open, high, low, close)) {
          priceStream.add(new PriceStreamElement(dateTime, open, high, low, close, close, close));
        }
      }
    } catch (SQLException e) {
      e.printStackTrace();
      System.exit(1);
    }
    refresh();
  }

  /**
   * @param open
   * @param high
   * @param low
   * @param close
   * @return A boolean indicating whether prices are valid
   */
  boolean priceDataIsValid(double open, double high, double low, double close) {
    return (high >= open) && (high >= low) && (high >= close)
    /**/&& (low <= open) && (low <= close);
  }

  @Override
  public SecurityType type() {
    return SecurityType.Index;
  }
}
