package edu.berkeley.sky.market;

import java.util.HashMap;
import java.util.Map;

/**
 * Enumerate various types of securities, e.g. stock, option.
 *
 * @author Agrim Pathak
 */
public enum SecurityType {
  /**/Futures,
  /**/Index,
  /**/Option,
  /**/Stock;

  public static Map<SecurityType, Integer> securityTypeOrdering =
  /**/new HashMap<SecurityType, Integer>();

  static {
    securityTypeOrdering.put(Futures, 2);
    securityTypeOrdering.put(Index, 1);
    securityTypeOrdering.put(Option, 3);
    securityTypeOrdering.put(Stock, 0);
  }
}
