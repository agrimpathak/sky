package edu.berkeley.sky.market;

import org.joda.time.MutableDateTime;

/**
 * A derivative a security that closely derives its value from another security, i.e. an underlying
 * security
 *
 * @author Agrim Pathak
 */
abstract class AbstractDerivative extends AbstractSecurity implements Derivative {

  private final int contractSize;
  private final Security underlying;

  AbstractDerivative(String ticker, MutableDateTime simDateTime, int contractSize,
      Security underlying) {
    super(ticker, simDateTime);
    this.contractSize = contractSize;
    this.underlying = underlying;
  }

  @Override
  public int contractSize() {
    return contractSize;
  }

  @Override
  public Security underlying() {
    return underlying;
  }
}
