package edu.berkeley.sky.market;

import static edu.berkeley.sky.util.DateUtil.stringToLocalDate;
import static edu.berkeley.sky.util.Preconditions.checkArgument;
import static edu.berkeley.sky.util.Preconditions.checkNotNull;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import org.joda.time.DateTime;
import org.joda.time.LocalDate;
import org.joda.time.MutableDateTime;

import edu.berkeley.sky.database.Dao;
import edu.berkeley.sky.database.Queries;

/**
 * Market class that acts as an interface against the price data present in the database. The client
 * is able to request securities from this object.
 *
 * @author Agrim Pathak
 */
public class Market implements Iterable<Security> {

  private static final int NUM_MINUTES_PER_FETCH = 15;
  private DateTime nextFetchTime;
  private final Map<String, List<LocalDate>> optTickerToExp;
  private final Queries queries;
  private final MutableDateTime simDateTime;
  private final Map<String, AbstractSecurity> tickerToSecurity =
  /**/new HashMap<String, AbstractSecurity>();

  public Market(Dao dao, boolean loadOptionData, MutableDateTime simDateTime) {
    checkArgument(dao.isConnected());
    queries = new Queries(dao);
    this.simDateTime = simDateTime;
    nextFetchTime = simDateTime.toDateTime().plusHours(NUM_MINUTES_PER_FETCH);
    if (loadOptionData) {
      optTickerToExp = new HashMap<String, List<LocalDate>>();
      fetchAvailableExpirations(dao);
    } else {
      optTickerToExp = null;
    }
  }

  /**
   * Remove all expired options from the optionsExistInDb map.
   *
   * @param underlyingTicker
   */
  private void clearExpiredOptions(String underlyingTicker) {
    Collection<LocalDate> availableExpirations =
    /**/optTickerToExp.get(underlyingTicker);
    if (availableExpirations == null) {
      return;
    }
    Iterator<LocalDate> iter = availableExpirations.iterator();
    while (iter.hasNext()) {
      if (iter.next().isBefore(simDateTime.toDateTime().toLocalDate())) {
        iter.remove();
      } else {
        break;
      }
    }
    if (availableExpirations.isEmpty()) {
      optTickerToExp.remove(underlyingTicker);
    }
  }

  /**
   * Query all available options in the database.
   */
  private void fetchAvailableExpirations(Dao dao) {
    LinkedList<LocalDate> expirations = null;
    String underlying, prevUnderlying = "";
    ResultSet rs =
    /**/dao.executeQuery(queries.availableOptionsQuery(simDateTime.toDateTime().toLocalDate()));
    try {
      while (rs.next()) {
        underlying = rs.getString(1);
        if (!underlying.equals(prevUnderlying)) {
          optTickerToExp.put(prevUnderlying, expirations);
          expirations = new LinkedList<LocalDate>();
          prevUnderlying = underlying;
        }
        expirations.add(stringToLocalDate(rs.getString(2)));
      }
    } catch (SQLException e) {
      e.printStackTrace();
      dao.disconnect();
      System.exit(1);
    }
    optTickerToExp.put(prevUnderlying, expirations);
    optTickerToExp.remove("");
  }

  /**
   * Query price data for the given securities and pass the Result Set to the security to fill
   *
   * @param security
   */
  private void fetchPriceData(AbstractSecurity security) {
    checkNotNull(security);
    security.fillPrice(queries.priceData(security, simDateTime, nextFetchTime));
  }

  /**
   * @param underlyingTicker
   * @return A list of expiration dates available for a given underlying security.
   */
  public List<LocalDate> getExpirations(String underlyingTicker) {
    clearExpiredOptions(underlyingTicker);
    return optTickerToExp.get(underlyingTicker);
  }

  /**
   * Return an iterator to actively tracked securities
   */
  /*
   * Creating a method to return tickerToSecurity.values() as Collection<Security> is not possible
   */
  @Override
  public Iterator<Security> iterator() {
    return new Iterator<Security>() {
      private final Iterator<AbstractSecurity> i = tickerToSecurity.values().iterator();

      @Override
      public boolean hasNext() {
        return i.hasNext();
      }

      @Override
      public Security next() {
        return i.next();
      }

      @Override
      public void remove() {
        i.remove();
      }
    };
  }

  /**
   * Refresh all of the market's security prices.
   */
  public void refresh() {
    if (simDateTime.isAfter(nextFetchTime)) {
      nextFetchTime = simDateTime.toDateTime().plusMinutes(NUM_MINUTES_PER_FETCH);
      for (AbstractSecurity security : tickerToSecurity.values()) {
        fetchPriceData(security);
      }
    } else {
      Iterator<AbstractSecurity> iter = tickerToSecurity.values().iterator();
      AbstractSecurity security;
      while (iter.hasNext()) {
        security = iter.next();
        if ((security.type() == SecurityType.Option) && ((Option) security).isExpired()) {
          iter.remove();
        } else {
          security.refresh();
        }
      }
    }
  }

  /**
   * Remove a security from the market.
   *
   * @param security
   */
  public void removeSecurity(Security security) {
    tickerToSecurity.remove(security.ticker());
  }

  /**
   * Request a security.
   *
   * @param security
   * @return
   */
  private Security request(AbstractSecurity security) {
    if (tickerToSecurity.containsKey(security.ticker())) {
      return tickerToSecurity.get(security.ticker());
    }
    tickerToSecurity.put(security.ticker(), security);
    fetchPriceData(security);
    return security;
  }

  /**
   * @param ticker
   * @param underlyingTicker
   * @param underlyingType
   * @return A futures contract
   */
  public Security requestFutures(String ticker, Security underlying, int contractSize) {
    return request(new Futures(ticker, simDateTime, contractSize, underlying));
  }

  /**
   * @param ticker
   * @return A market index
   */
  public Security requestIndex(String ticker) {
    return request(new Index(ticker, simDateTime));
  }

  /**
   * @param underlyingTicker
   * @param expiration
   * @param strike
   * @param optionType
   * @return An option
   */
  public Security requestOption(Security underlying, LocalDate expiration, double strike,
      OptionType optionType) {
    return request(new Option(simDateTime, underlying, expiration, strike, optionType));
  }

  /**
   * @param ticker
   * @return A stock or ETF
   */
  public Security requestStock(String ticker) {
    return request(new Stock(ticker, simDateTime));
  }

  /**
   * @return A collection of tickers from which price data is readily available
   */
  public Collection<String> watchlist() {
    return tickerToSecurity.keySet();
  }
}
