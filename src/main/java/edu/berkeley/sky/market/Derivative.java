package edu.berkeley.sky.market;

/**
 * A derivative is a security that derives its value from another security, called the underlying.
 * 
 * @author Agrim Pathak
 */
public interface Derivative {

  /**
   * @return The number of units (e.g. shares, contracts) the derivative represents of the
   *         underlying.
   */
  int contractSize();

  /**
   * @return The derivatives underlying security.
   */
  Security underlying();
}
