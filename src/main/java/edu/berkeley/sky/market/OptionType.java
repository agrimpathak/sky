package edu.berkeley.sky.market;

/**
 * Option Type enum, e.g. Call or Put.
 *
 * @author Agrim Pathak
 */
public enum OptionType {

  /**/CALL('C'),
  /**/PUT('P');

  private char name;

  OptionType(char name) {
    this.name = name;
  }

  public char getName() {
    return name;
  }

  public int sign() {
    return this == CALL ? 1 : -1;
  }
}
