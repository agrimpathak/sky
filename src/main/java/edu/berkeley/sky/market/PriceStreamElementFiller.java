package edu.berkeley.sky.market;

import org.joda.time.DateTime;

/**
 * DEPRECATED
 * 
 * A cluster of bid, ask and trade price bars at a specific time. Fills in missing prices according
 * to a spread.
 * 
 * @author Agrim Pathak
 */
public class PriceStreamElementFiller {

  private final double ask;
  private final double bid;
  private final double last;
  private final double spread;
  private final DateTime timestamp;

  PriceStreamElementFiller(DateTime timestamp, double bid, double ask, double last, double spread) {
    this.timestamp = timestamp;
    this.bid = bid;
    this.ask = ask;
    this.last = last;
    this.spread = spread;
  }

  /**
   * @return Return the ask price
   */
  double getAsk() {
    if (ask != 0) {
      return ask;
    } else if (bid != 0) {
      return bid + spread;
    } else if (last != 0) {
      return last + spread;
    } else {
      return 0.0;
    }
  }

  /**
   * @return Return the bid price
   */
  double getBid() {
    if (bid != 0) {
      return bid;
    } else if (ask != 0) {
      return ask - spread;
    } else if (last != 0) {
      return last - spread;
    } else {
      return 0.0;
    }
  }

  /**
   * @return Return the last trade price
   */
  double getLast() {
    return last;
  }

  /**
   * @return The DateTime at which the prices are reflective of.
   */
  DateTime getTimestamp() {
    return timestamp;
  }
}
