package edu.berkeley.sky.market;

import static edu.berkeley.sky.util.MathUtil.average;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.LinkedList;

import org.joda.time.DateTime;
import org.joda.time.Minutes;
import org.joda.time.MutableDateTime;

/**
 * Abstract security class
 *
 * @author Agrim Pathak
 */
abstract class AbstractSecurity implements Security {

  private PriceStreamElement currentPriceStreamElem;
  protected final LinkedList<PriceStreamElement> priceStream =
  /**/new LinkedList<PriceStreamElement>();
  protected final MutableDateTime simDateTime;
  private final String ticker;

  AbstractSecurity(String ticker, MutableDateTime simDateTime) {
    this.ticker = ticker;
    this.simDateTime = simDateTime;
  }

  @Override
  public double ask() {
    return currentPriceStreamElem == null ? 0.0 : currentPriceStreamElem.ask();
  }

  @Override
  public double bid() {
    return currentPriceStreamElem == null ? 0.0 : currentPriceStreamElem.bid();
  }

  @Override
  public double bidAskSpread() {
    return currentPriceStreamElem == null ? Double.MAX_VALUE :
    /**/currentPriceStreamElem.ask() - currentPriceStreamElem.bid();
  }

  @Override
  public double close() {
    return currentPriceStreamElem == null ? 0.0 : currentPriceStreamElem.close();
  }

  @Override
  public boolean equals(Object obj) {
    if (obj == null) {
      return false;
    }
    if (obj == this) {
      return true;
    }
    if (!(obj instanceof AbstractSecurity)) {
      return false;
    }
    return ticker.equals(((AbstractSecurity) obj).ticker);
  }

  /**
   * Fill price data given the result set
   *
   * @param rs
   */
  void fillPrice(ResultSet rs) {
    priceStream.clear();
    DateTime dateTime;
    try {
      while (rs.next()) {
        dateTime = new DateTime(rs.getTimestamp("price_timestamp"));
        double open = rs.getDouble("open");
        double high = rs.getDouble("high");
        double low = rs.getDouble("low");
        double close = rs.getDouble("close");
        double bid = rs.getDouble("close_bid");
        double ask = rs.getDouble("close_ask");
        if (priceDataIsValid(open, high, low, close, bid, ask)) {
          priceStream.add(new PriceStreamElement(dateTime, open, high, low, close, bid, ask));
        }
      }
    } catch (SQLException e) {
      e.printStackTrace();
      System.exit(1);
    }
    refresh();
  }

  @Override
  public int hashCode() {
    return ticker.hashCode();
  }

  @Override
  public boolean hasPriceData() {
    return currentPriceStreamElem != null;
  }

  @Override
  public double high() {
    return currentPriceStreamElem == null ? 0.0 : currentPriceStreamElem.high();
  }

  @Override
  public double low() {
    return currentPriceStreamElem == null ? 0.0 : currentPriceStreamElem.low();
  }

  @Override
  public double mid() {
    return currentPriceStreamElem == null ? 0.0 :
    /**/average(currentPriceStreamElem.bid(), currentPriceStreamElem.ask());
  }

  @Override
  public int minutesSinceTimestamp() {
    return currentPriceStreamElem == null ? Integer.MAX_VALUE :
    /**/Minutes.minutesBetween(currentPriceStreamElem.timestamp(), simDateTime).getMinutes();
  }

  @Override
  public int minutesToNextTimestamp() {
    return priceStream.size() == 0 ? Integer.MAX_VALUE :
    /**/Minutes.minutesBetween(simDateTime, priceStream.peek().timestamp()).getMinutes();
  }

  @Override
  public double open() {
    return currentPriceStreamElem == null ? 0.0 : currentPriceStreamElem.open();
  }

  /**
   * @param open
   * @param high
   * @param low
   * @param close
   * @param bid
   * @param ask
   * @return A boolean indicating whether prices are valid
   */
  private boolean priceDataIsValid(double open, double high, double low, double close, double bid,
      double ask) {
    return (bid > 0) && (ask > 0) && (bid < ask)
    /**/&& (high >= open) && (high >= low) && (high >= close)
    /**/&& (low <= open) && (low <= close);
  }

  /**
   * Refresh price data, i.e. ensure price data is most recent up to and including the current
   * simulation time.
   */
  void refresh() {
    if ((currentPriceStreamElem != null) &&
    /* */currentPriceStreamElem.timestamp().equals(simDateTime)) {
      return;
    }
    if (priceStream.isEmpty()) {
      return;
    }
    while (priceStream.peek().timestamp().isBefore(simDateTime) &&
    /*  */(priceStream.size() > 1) &&
    /*  */!priceStream.get(1).timestamp().isAfter(simDateTime)) {
      priceStream.poll();
    }
    if (!priceStream.peek().timestamp().isAfter(simDateTime)) {
      currentPriceStreamElem = priceStream.peek();
    }
  }

  @Override
  public String ticker() {
    return ticker;
  }

  @Override
  public DateTime timestamp() {
    return currentPriceStreamElem == null ? null : currentPriceStreamElem.timestamp();
  }

  @Override
  public String toString() {
    return ticker;
  }
}
