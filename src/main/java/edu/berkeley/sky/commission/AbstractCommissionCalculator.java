package edu.berkeley.sky.commission;

import edu.berkeley.sky.market.Futures;
import edu.berkeley.sky.market.Option;
import edu.berkeley.sky.market.Security;
import edu.berkeley.sky.market.Stock;

/**
 * 
 * Abstract class for commission calculators
 * 
 * @author Agrim Pathak
 */
abstract class AbstractCommissionCalculator implements CommissionCalculator {

  @Override
  public double calculate(Security security, int size) {
    switch (security.type()) {
      case Stock:
        return calculate((Stock) security, size);
      case Option:
        return calculate((Option) security, size);
      case Futures:
        return calculate((Futures) security, size);
      case Index:
          return 0;
      default:
        throw new IllegalArgumentException();
    }
  }
}
