package edu.berkeley.sky.commission;

import static java.lang.Math.abs;
import static java.lang.Math.max;
import edu.berkeley.sky.market.Futures;
import edu.berkeley.sky.market.Option;
import edu.berkeley.sky.market.Stock;

/**
 * Interactive Brokers commission calculator
 * 
 * @author Agrim Pathak
 */
public class InteractiveBrokersCommission extends AbstractCommissionCalculator {

  @Override
  public double calculate(Futures futures, int size) {
    return (1.15 + 0.85) * abs(size);
  }

  @Override
  public double calculate(Option option, int size) {
    if (option.ask() >= 0.10) {
      return abs(size) * 0.70;
    } else if (option.ask() >= 0.05) {
      return abs(size) * 0.50;
    } else {
      return abs(size) * 0.25;
    }
  }

  @Override
  public double calculate(Stock stock, int size) {
    return max(abs(size) * 0.005, 1.0);
  }
}
