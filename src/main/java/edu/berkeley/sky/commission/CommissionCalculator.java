package edu.berkeley.sky.commission;

import edu.berkeley.sky.market.Futures;
import edu.berkeley.sky.market.Option;
import edu.berkeley.sky.market.Security;
import edu.berkeley.sky.market.Stock;

/**
 * Trade commission calculator
 * 
 * @author Agrim Pathak
 */
public interface CommissionCalculator {

  double calculate(Futures futures, int size);

  double calculate(Option option, int size);

  double calculate(Security security, int size);

  double calculate(Stock stock, int size);
}
