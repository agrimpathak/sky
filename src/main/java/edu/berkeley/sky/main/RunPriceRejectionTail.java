package edu.berkeley.sky.main;

import static edu.berkeley.sky.util.DateUtil.MARKET_HOURS;

import java.util.ArrayList;
import java.util.List;

import org.joda.time.DateTime;
import org.joda.time.MutableDateTime;

import edu.berkeley.sky.algo.Algo;
import edu.berkeley.sky.algo.PriceRejectionTail;
import edu.berkeley.sky.algo.Trader;
import edu.berkeley.sky.algo.param.PriceRejectionTailParams;
import edu.berkeley.sky.commission.InteractiveBrokersCommission;
import edu.berkeley.sky.database.Dao;
import edu.berkeley.sky.market.Market;
import edu.berkeley.sky.market.Security;
import edu.berkeley.sky.position.Portfolio;
import edu.berkeley.sky.report.Recorder;
import edu.berkeley.sky.report.pnl.PnlRecorder;
import edu.berkeley.sky.util.DateUtil;

/**
 * @author Agrim Pathak
 */
public class RunPriceRejectionTail {

  public static void main(String[] args) {
    // Simulation start/end dates
    MutableDateTime simDateTime = DateUtil.dateTimeBuilder()
    /**/.withYear(2014).withMonth(5).withDay(12)
    /**/.withHour(13).withMinute(30)
    /**/.buildMutable();
    DateTime simEndDateTime = DateUtil.dateTimeBuilder()
    /**/.withYear(2014).withMonth(11).withDay(7)
    /**/.withHour(20).withMinute(0)
    /**/.build();

    // Dao and market
    Dao dao = Dao.defaultDao();
    dao.connect();
    Market market = new Market(dao, false, simDateTime);
    Security spx = market.requestIndex("SPX INDEX");
    Security es = market.requestFutures("ESM4", spx, 50);

    Portfolio portfolio = new Portfolio(new InteractiveBrokersCommission());

    PriceRejectionTailParams params = PriceRejectionTailParams.builder()
    /**/.withInstrument(es)
    /**/.withLookBackMinutes(15)
    /**/.withMinPriceRange(2.50)
    /**/.withProfitTargetFactorOfBarRange(1.0)
    /**/.withStopLossFactorOfBarRange(1.0)
    /**/.withTailRatio(0.6)
    /**/.withMarket(market)
    /**/.withPortfolio(portfolio)
    /**/.withSimulationDateTime(simDateTime)
    /**/.withPositionTimeInterval(MARKET_HOURS)
    /**/.withTradeTimeInterval(MARKET_HOURS)
    /**/.build();

    // Recorders
    List<Recorder> recorders = new ArrayList<Recorder>();
    recorders.add(new PnlRecorder(simDateTime, "pnl.csv", market, portfolio));

    // Strategies
    List<Algo> strategies = new ArrayList<Algo>();
    strategies.add(new PriceRejectionTail(params));
    // strategies.add(new SpreadAlgo(spreadParamsBuilder1.build()));
    Trader trader = new Trader(strategies);

    // Run
    Simulator
    /**/.runMarketHours(simDateTime, simEndDateTime, market, dao, trader, portfolio, recorders);
  }
}
