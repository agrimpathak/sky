package edu.berkeley.sky.main;

import static edu.berkeley.sky.util.DateUtil.ALL_HOURS;
import static edu.berkeley.sky.util.DateUtil.MARKET_HOURS;

import java.util.ArrayList;
import java.util.List;

import org.joda.time.DateTime;
import org.joda.time.MutableDateTime;

import edu.berkeley.sky.algo.Algo;
import edu.berkeley.sky.algo.DeltaNeutralHedge;
import edu.berkeley.sky.algo.SpreadAlgo;
import edu.berkeley.sky.algo.Trader;
import edu.berkeley.sky.algo.param.DNHedgeParams;
import edu.berkeley.sky.algo.param.SpreadParams;
import edu.berkeley.sky.algo.param.SpreadParamsBuilder;
import edu.berkeley.sky.analytics.RelativePriceDelta;
import edu.berkeley.sky.commission.InteractiveBrokersCommission;
import edu.berkeley.sky.database.Dao;
import edu.berkeley.sky.market.Market;
import edu.berkeley.sky.market.OptionType;
import edu.berkeley.sky.market.Security;
import edu.berkeley.sky.math.LinearTransform;
import edu.berkeley.sky.position.Portfolio;
import edu.berkeley.sky.position.SpreadBuilder;
import edu.berkeley.sky.report.Recorder;
import edu.berkeley.sky.report.pnl.PnlRecorder;
import edu.berkeley.sky.util.DateUtil;
import edu.berkeley.sky.util.OptionUtil;

/**
 *
 * @author Agrim Pathak
 */
public class RunSpread {

  public static void main(String[] args) {
	 OptionUtil.optionPriceMultiple = 5;
	  
    // Simulation start/end dates
    MutableDateTime simDateTime = DateUtil.dateTimeBuilder()
    /**/.withYear(2014).withMonth(6).withDay(16)
    /**/.withHour(6).withMinute(30)
    /**/.buildMutable();
    DateTime simEndDateTime = DateUtil.dateTimeBuilder()
    /**/.withYear(2015).withMonth(6).withDay(26)
    /**/.withHour(13).withMinute(0)
    /**/.build();

    // Dao and market
    Dao dao = Dao.defaultDao();
    dao.connect();
    Market market = new Market(dao, true, simDateTime);
    Security underlying = market.requestIndex("SPX INDEX");
    Security hedgingInstrument = market.requestStock("SPY US EQUITY");

    // SpreadBuilder and params
    SpreadBuilder spreadBuilder0 = new SpreadBuilder()
    /**/.withMarket(market)
    /**/.withUnderlying(underlying)
    /**/.withExpirationWeek0(0)
    /**/.withExpirationWeek1(0)
    /**/.withOptionType0(OptionType.CALL)
    /**/.withOptionType1(OptionType.PUT)
    /**/.withOtmAmount0(40)
    /**/.withOtmAmount1(40)
    /**/.withSize0(-1)
    /**/.withSize1(-1);

    SpreadBuilder spreadBuilder1 = spreadBuilder0.clone()
    /**/.withOptionType0(OptionType.PUT)
    /**/.withOptionType1(OptionType.PUT);
    
    Portfolio portfolio = new Portfolio(new InteractiveBrokersCommission());

    SpreadParamsBuilder spreadParamsBuilder0 = SpreadParams.builder()
    /**/.withSpreadBuilder(spreadBuilder0)
    /**/.withUnderlying(underlying)
    /**/.withMillisBeforeExpiration(0L * 60L * 1000L)
    /**/.withMinOtmAmountBeforeExit(-5.0)
    /**/.withMinPrice(0.00)
    /**/.withMaxBidAskSpreadOnEntry(0.30)
    /**/.withMarket(market)
    /**/.withPortfolio(portfolio)
    /**/.withSimulationDateTime(simDateTime)
    /**/.withPositionTimeInterval(ALL_HOURS)
    /**/.withTradeTimeInterval(MARKET_HOURS);
    SpreadParamsBuilder spreadParamsBuilder1 = spreadParamsBuilder0.clone()
    /**/.withSpreadBuilder(spreadBuilder1);
    DNHedgeParams hedgeParams = DNHedgeParams.builder()
    /**/.cloneBase(spreadParamsBuilder0)
    /**/.withDeltaCalculator(new RelativePriceDelta(market))
    /**/.withHedgeInstrument(hedgingInstrument)
    /**/.withMaxUnhedgedDelta(300)
    /**/.withMinimumHedgeSize(100)
    /**/.withDeltaAdjustmentFunction(new LinearTransform(10.0, 0.0))
    /**/.build();

    // Recorders
    List<Recorder> recorders = new ArrayList<Recorder>();
    recorders.add(new PnlRecorder(simDateTime, "pnl.csv", market, portfolio));

    // Strategies
    List<Algo> strategies = new ArrayList<Algo>();
    strategies.add(new SpreadAlgo(spreadParamsBuilder0.build()));
    //	strategies.add(new SpreadAlgo(spreadParamsBuilder1.build()));
    //	strategies.add(new DeltaNeutralHedge(hedgeParams));
    Trader trader = new Trader(strategies);

    // Run
    Simulator
    /**/.runMarketHours(simDateTime, simEndDateTime, market, dao, trader, portfolio, recorders);
  }
}
