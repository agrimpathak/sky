package edu.berkeley.sky.main;

import java.util.List;

import org.joda.time.DateTime;
import org.joda.time.MutableDateTime;

import edu.berkeley.sky.algo.Trader;
import edu.berkeley.sky.database.Dao;
import edu.berkeley.sky.market.Market;
import edu.berkeley.sky.position.Portfolio;
import edu.berkeley.sky.report.Exporter;
import edu.berkeley.sky.report.Recorder;
import edu.berkeley.sky.util.TimeInterval;
import static edu.berkeley.sky.util.DateUtil.ALL_HOURS;
import static edu.berkeley.sky.util.DateUtil.DATE_TIME_FORMAT;
import static edu.berkeley.sky.util.DateUtil.MARKET_HOURS;
import static edu.berkeley.sky.util.DateUtil.isWeekend;

/**
 *
 * @author Agrim Pathak
 */
public class Simulator {

  static void run(MutableDateTime simDateTime, DateTime simEndDateTime, Market market, Dao dao,
      Trader trader, Portfolio portfolio, List<Recorder> recorders) {
    run(simDateTime, simEndDateTime, market, dao, trader, portfolio, recorders, ALL_HOURS);
  }

  static void run(MutableDateTime simDateTime, DateTime simEndDateTime, Market market, Dao dao,
      Trader trader, Portfolio portfolio, List<Recorder> recorders,
      TimeInterval simulationTimeInterval) {
    System.out.println(simDateTime.toString(DATE_TIME_FORMAT));
    while (simDateTime.isBefore(simEndDateTime)) {
      if (isWeekend(simDateTime)) {
        System.out.println(simDateTime.toString(DATE_TIME_FORMAT));
        simDateTime.addDays(1);
        continue;
      }
      simDateTime.addMinutes(1);
      if (!simulationTimeInterval.contains(simDateTime)) {
        // TODO: Skip simDateTime to beginning of simulationTimeInterval
        continue;
      }
      if (simDateTime.getMinuteOfHour() == 0) {
        System.out.println(simDateTime.toString(DATE_TIME_FORMAT));
      }
      market.refresh();
      if (portfolio != null){
    	portfolio.updateUnrealizedPnl();
      }
      trader.trade();
      for (Recorder recorder : recorders) {
        recorder.record();
      }
      if (portfolio != null){
    	portfolio.removeStaged();
      }
    }
    if (portfolio != null) {
      portfolio.liquidate();
      portfolio.updateUnrealizedPnl();
    }
    for (Recorder recorder : recorders) {
      recorder.record();
    }
    dao.disconnect();
    for (Recorder recorder : recorders) {
      Exporter.writeCsv(recorder.flushRecords(), recorder.path());
    }
    System.out.println("Done");
  }

  static void runMarketHours(MutableDateTime simDateTime, DateTime simEndDateTime, Market market,
      Dao dao, Trader trader, Portfolio portfolio, List<Recorder> recorders) {
    run(simDateTime, simEndDateTime, market, dao, trader, portfolio, recorders, MARKET_HOURS);
  }
}
