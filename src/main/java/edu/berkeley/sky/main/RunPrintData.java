package edu.berkeley.sky.main;

import java.util.ArrayList;
import java.util.List;

import org.joda.time.DateTime;
import org.joda.time.MutableDateTime;

import edu.berkeley.sky.algo.Algo;
import edu.berkeley.sky.algo.DataPrinter;
import edu.berkeley.sky.algo.Trader;
import edu.berkeley.sky.algo.param.OptionDataPrinterParams;
import edu.berkeley.sky.database.Dao;
import edu.berkeley.sky.market.Market;
import edu.berkeley.sky.report.Recorder;
import edu.berkeley.sky.report.data.DataRecorder;
import edu.berkeley.sky.report.data.extractor.BidAskSpreadExtractor;
import edu.berkeley.sky.report.data.extractor.BlackScholesImpliedVolatilityDeltaExtractor;
import edu.berkeley.sky.report.data.extractor.ImpliedVolatilityExtractor;
import edu.berkeley.sky.report.data.extractor.MidPriceExtractor;
import edu.berkeley.sky.report.data.extractor.RelativePriceDeltaExtractor;
import edu.berkeley.sky.util.DateUtil;
import edu.berkeley.sky.util.OptionUtil;

/**
 *
 * @author Agrim Pathak
 */
public class RunPrintData {

  public static void main(String[] args) {
    // Simulation start/end dates
    MutableDateTime simDateTime = DateUtil.dateTimeBuilder()
    /**/.withYear(2014).withMonth(6).withDay(16)
    /**/.withHour(13).withMinute(30)
    /**/.buildMutable();
    DateTime simEndDateTime = DateUtil.dateTimeBuilder()
    /**/.withYear(2014).withMonth(6).withDay(19)
    /**/.withHour(20).withMinute(0)
    /**/.build();

    // Dao and market
    Dao dao = Dao.defaultDao();
    dao.connect();
    Market market = new Market(dao, true, simDateTime);

    // Params
    OptionDataPrinterParams params = OptionDataPrinterParams.builder()
    /**/.withExpiration(simEndDateTime.toLocalDate())
    /**/.withMarket(market)
    /**/.withStrikeInterval(5.0)
    /**/.withStrikeLowerLimit(80.0)
    /**/.withStrikeUpperLimit(80.0)
    /**/.withUnderlying(market.requestStock("SPX INDEX"))
    /**/.build();

    // Recorders
    List<Recorder> recorders = new ArrayList<Recorder>();

    recorders.add(new DataRecorder<MidPriceExtractor>(
    /**/simDateTime, "mid.csv", market, new MidPriceExtractor(7), 15));

    recorders.add(new DataRecorder<ImpliedVolatilityExtractor>(
    /**/simDateTime, "iv.csv", market, new ImpliedVolatilityExtractor(7), 15));

    recorders.add(new DataRecorder<BidAskSpreadExtractor>(
    /**/simDateTime, "bidaskspread.csv", market, new BidAskSpreadExtractor(7), 15));

    recorders.add(new DataRecorder<BlackScholesImpliedVolatilityDeltaExtractor>(
    /**/simDateTime, "bsiv_delta.csv", market,
    /**/new BlackScholesImpliedVolatilityDeltaExtractor(7), 15));

    recorders.add(new DataRecorder<RelativePriceDeltaExtractor>(
    /**/simDateTime, "rel_delta.csv", market,
    /**/new RelativePriceDeltaExtractor(7, market), 15));

    // Strategies
    OptionUtil.optionPriceMultiple = 5;
    List<Algo> strategies = new ArrayList<Algo>(1);
    strategies.add(new DataPrinter(params));
    Trader trader = new Trader(strategies);

    // Run
    Simulator.runMarketHours(simDateTime, simEndDateTime, market, dao, trader, null, recorders);
  }
}
