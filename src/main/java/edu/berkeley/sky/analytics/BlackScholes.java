package edu.berkeley.sky.analytics;

import org.apache.commons.math3.distribution.NormalDistribution;

import edu.berkeley.sky.market.Option;
import edu.berkeley.sky.market.OptionType;
import static edu.berkeley.sky.util.MathUtil.square;
import static java.lang.Math.exp;
import static java.lang.Math.log;
import static java.lang.Math.max;
import static java.lang.Math.sqrt;

/**
 * Calculates theoretical price and Greeks of an option via Black Scholes. All mathematical symbols
 * mimic ones here: http://en.wikipedia.org/wiki/Black-Scholes.
 * <p/>
 * Greek explanations:
 * <p/>
 * 1) Price: Theoretical price of a European-style option.
 * <p/>
 * 2) Delta: If underlying changes by $x, price of an option changes by delta * $x.
 * <p/>
 * 3) Gamma: If underlying changes by $x, delta changes by gamma * x.
 * <p/>
 * 4) Theta: Option time-decay, annualized.
 * <p/>
 * 5) Vega: If underlying annualized volatility changes by x (e.g. 0.01, e.g. 11% to 12%), option
 * price changes by vega * x.
 * <p/>
 * 6) Rho: If the annualized risk-free rate changes by x (e.g. 0.005, e.g. 1% to 1.5%), option price
 * changes by rho * x.
 *
 * @author Agrim Pathak
 */
public class BlackScholes implements DeltaCalculator {

  private final static double DEFAULT_RISK_FREE_RATE = 0.02;
  private final static double DEFAULT_VOLATILITY = 0.11;
  private final static NormalDistribution normal = new NormalDistribution();
  private final double defaultRiskFreeRate;
  private final double defaultVolatility;

  public BlackScholes() {
    this(DEFAULT_VOLATILITY, DEFAULT_RISK_FREE_RATE);
  }

  public BlackScholes(double defaultVolatility, double defaultRiskFreeRate) {
    this.defaultVolatility = defaultVolatility;
    this.defaultRiskFreeRate = defaultRiskFreeRate;
  }

  /** Helper values d1 and d2 used repeatedly **/
  private double[] calculateD(double S, double K, double T, double sigma, double r) {
    double d1 = (log(S / K) + ((r + (square(sigma) / 2.0)) * T)) / sigma / sqrt(T);
    double d2 = d1 - (sigma * sqrt(T));
    return new double[] {d1, d2};
  }

  /* DELTA */
  public double delta(double S, double K, double T, double sigma, double r, boolean call) {
    if (T <= 0) {
      if (call) {
        return (S > K) ? 1.0 : 0.0;
      } else {
        return (S < K) ? 1.0 : 0.0;
      }
    }
    double d1 = calculateD(S, K, T, sigma, r)[0];
    double callDelta = normal.cumulativeProbability(d1);
    if (call) {
      return callDelta;
    }
    return callDelta - 1.0;
  }

  @Override
  public double delta(Option option) {
    return delta(option.underlying().mid(), option.strike(), option.yearsToExpiration(),
        defaultVolatility, defaultRiskFreeRate, option.optionType() == OptionType.CALL);
  }

  /* GAMMA */
  public double gamma(double S, double K, double T, double sigma, double r) {
    if (T == 0) {
      return 0.0; // infinity at strike, 0 everywhere else
    }
    double d1 = calculateD(S, K, T, sigma, r)[0];
    return normal.density(d1) / S / sigma / sqrt(T);
  }

  /* PRICE */
  public double price(double S, double K, double T, double sigma, double r, boolean call) {
    if (T == 0) {
      if (call) {
        return max(0, S - K);
      } else {
        return max(0, K - S);
      }
    }
    double[] d = calculateD(S, K, T, sigma, r);
    double d1 = d[0];
    double d2 = d[1];
    double callPrice = (normal.cumulativeProbability(d1) * S)
    /*             */- (normal.cumulativeProbability(d2) * K * exp(-r * T));
    if (call) {
      return callPrice;
    }
    return (callPrice - S) + (K * exp(-r * T));
  }

  /* RHO */
  public double rho(double S, double K, double T, double sigma, double r, boolean call) {
    if (T == 0) {
      return 0.0;
    }
    double d2 = calculateD(S, K, T, sigma, r)[1];
    if (call) {
      return K * T * exp(-r * T) * normal.cumulativeProbability(d2);
    }
    return -K * T * exp(-r * T) * normal.cumulativeProbability(-d2);
  }

  /* THETA */
  public double theta(double S, double K, double T, double sigma, double r, boolean call) {
    if (T == 0) {
      return 0.0;
    }
    double[] d = calculateD(S, K, T, sigma, r);
    double d1 = d[0];
    double d2 = d[1];
    double firstTerm = (-S * normal.density(d1) * sigma) / 2.0 / sqrt(T);
    if (call) {
      double callSecondTerm = -r * K * exp(-r * T) * normal.cumulativeProbability(d2);
      return firstTerm + callSecondTerm;
    }
    double putSecondTerm = r * K * exp(-r * T) * normal.cumulativeProbability(-d2);
    return firstTerm + putSecondTerm;
  }

  public double theta(Option option) {
    return theta(option.underlying().mid(), option.strike(), option.yearsToExpiration(),
        defaultVolatility, defaultRiskFreeRate, option.optionType() == OptionType.CALL);
  }

  /* VEGA */
  public double vega(double S, double K, double T, double sigma, double r) {
    if (T == 0) {
      return 0.0;
    }
    double d1 = calculateD(S, K, T, sigma, r)[0];
    return S * normal.density(d1) * sqrt(T);
  }
  
  public double vega(Option option) {
    return vega(option.underlying().mid(), option.strike(), option.yearsToExpiration(),
        defaultVolatility, defaultRiskFreeRate);
  }
}
