package edu.berkeley.sky.analytics;

import edu.berkeley.sky.market.Market;
import edu.berkeley.sky.market.Option;
import edu.berkeley.sky.market.Security;
import edu.berkeley.sky.util.OptionUtil;

/**
 * Delta calculator that calculates delta of an option based on the prices of other options with
 * similar strike
 *
 * @author Agrim Pathak
 */
public class RelativePriceDelta implements DeltaCalculator {

  private final Market market;

  public RelativePriceDelta(Market market) {
    this.market = market;
  }

  /* DELTA */
  @Override
  public double delta(Option option) {
	int interval = OptionUtil.optionPriceMultiple;
    Security optionLow = market.requestOption(option.underlying(),
    /**/option.expiration(), -interval + option.strike(), option.optionType());
    Security optionHigh = market.requestOption(option.underlying(),
    /**/option.expiration(), +interval + option.strike(), option.optionType());
    return (optionLow.mid() - optionHigh.mid()) / (2 * interval);
  }
}
