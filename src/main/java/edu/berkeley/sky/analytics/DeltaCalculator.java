package edu.berkeley.sky.analytics;

import edu.berkeley.sky.market.Option;

/**
 * Interfaces of classes that can calculate an option's delta.
 * 
 * @author Agrim Pathak
 */
public interface DeltaCalculator {

  /**
   * @param option
   * @return An option's delta
   */
  double delta(Option option);
}
