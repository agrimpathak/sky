package edu.berkeley.sky.analytics;

import edu.berkeley.sky.market.Derivative;
import edu.berkeley.sky.market.Option;
import edu.berkeley.sky.market.Security;
import edu.berkeley.sky.market.SecurityType;
import edu.berkeley.sky.position.Portfolio;
import edu.berkeley.sky.position.Position;

/**
 * Calculate Greeks of a portfolio based on BlackScholesImpliedVolatility. Methods assume all
 * securities in a portfolio are related.
 *
 * @author Agrim Pathak
 */
public class PortfolioAnalytics {

  /* DELTA */
  public static double delta(Portfolio portfolio, DeltaCalculator calc) {
    double cumulDelta = 0;
    for (Position position : portfolio.positions()) {
      cumulDelta += delta(position, calc);
    }
    return cumulDelta;
  }

  public static double delta(Position position, DeltaCalculator calc) {
    Security security = position.security();
    if (security.type() == SecurityType.Option) {
      Option opt = (Option) security;
      return position.size() * opt.contractSize() * calc.delta(opt);
    } else if (security instanceof Derivative) {
      return position.size() * ((Derivative) security).contractSize();
    } else {
      return position.size();
    }
  }

  public static double optionDelta(Portfolio portfolio, DeltaCalculator calc) {
    double cumulDelta = 0;
    for (Position position : portfolio.positions()) {
      if (position.security().type() == SecurityType.Option) {
        cumulDelta += delta(position, calc);
      }
    }
    return cumulDelta;
  }
}
