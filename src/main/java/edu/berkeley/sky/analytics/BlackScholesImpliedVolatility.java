package edu.berkeley.sky.analytics;

import edu.berkeley.sky.market.Option;
import edu.berkeley.sky.market.OptionType;
import static edu.berkeley.sky.util.Constants.RISK_FREE_RATE;
import static edu.berkeley.sky.util.DateUtil.MINUTES_PER_YEAR;
import static edu.berkeley.sky.util.Preconditions.checkArgument;
import static java.lang.Math.abs;
import static java.lang.Math.max;

/**
 * Calculates theoretical price and Greeks of an option via Black Scholes. All mathematical symbols
 * mimic ones here: http://en.wikipedia.org/wiki/Black-Scholes. Instead of inputting volatility like
 * in BlackScholes, BlackScholesImpliedVolatility computes an implied volatility based on the price
 * of the option and uses that as input to bs.
 * <p/>
 * Greek explanations:
 * <p/>
 * 1) Price: Theoretical price of a European-style option.
 * <p/>
 * 2) Delta: If underlying changes by $x, price of an option changes by delta * $x.
 * <p/>
 * 3) Gamma: If underlying changes by $x, delta changes by gamma * x.
 * <p/>
 * 4) Theta: Option time-decay, annualized.
 * <p/>
 * 5) Vega: If underlying annualized volatility changes by x (e.g. 0.01, e.g. 11% to 12%), option
 * price changes by vega * x.
 * <p/>
 * 6) Rho: If the annualized risk-free rate changes by x (e.g. 0.005, e.g. 1% to 1.5%), option price
 * changes by rho * x.
 *
 * @author Agrim Pathak
 */
public class BlackScholesImpliedVolatility implements DeltaCalculator {

  private static final BlackScholes bs = new BlackScholes();
  private final static double INIT_GUESS = 0.1;
  private final static double MAX_ITERATIONS = 25;
  private final static double MAX_TOLERABLE_ERROR = 5E-3;
  private final static double MINIMUM_EFFECTIVE_T = 0.0 * (1.0 / MINUTES_PER_YEAR);
  private final static double TARGET_MAX_ERROR = 1E-4;

  public double delta(double V, double S, double K, double T, double r, boolean call) {
    if (T <= MINIMUM_EFFECTIVE_T) {
      if (call) {
        return (S > K) ? 1.0 : 0.0;
      } else {
        return (S < K) ? 1.0 : 0.0;
      }
    }
    double iv = impliedVolatility(V, S, K, T, r, call);
    return bs.delta(S, K, T, iv, r, call);
  }

  @Override
  public double delta(Option option) {
    double iv = impliedVolatility(option);
    double S = option.underlying().mid();
    double K = option.strike();
    double T = option.yearsToExpiration();
    boolean call = option.optionType() == OptionType.CALL;
    return bs.delta(S, K, T, iv, RISK_FREE_RATE, call);
  }

  /* GAMMA */
  public double gamma(double V, double S, double K, double T, double r, boolean call) {
    if (T <= MINIMUM_EFFECTIVE_T) {
      return 0.0;
    }
    double iv = impliedVolatility(V, S, K, T, r, call);
    return bs.gamma(S, K, T, iv, r);
  }

  /* DELTA */
  public double gamma(Option option) {
    double iv = impliedVolatility(option);
    double S = option.underlying().mid();
    double K = option.strike();
    double T = option.yearsToExpiration();
    return bs.gamma(S, K, T, iv, RISK_FREE_RATE);
  }

  /* IMPLIED VOLATILITY */
  public double impliedVolatility(double V, double S, double K, double T, boolean call) {
    return impliedVolatility(V, S, K, T, RISK_FREE_RATE, call);
  }

  public double impliedVolatility(double V, double S, double K, double T, double r, boolean call) {
    checkArgument(S > 0.0, K > 0.0, r >= 0.0);
    double iv = INIT_GUESS;
    int numIterations = 0;
    double error = bs.price(S, K, T, iv, r, call) - V;
    double vega = max(bs.vega(S, K, T, iv, r), Double.MIN_VALUE);
    do {
      iv = iv - (error / vega);
      vega = max(bs.vega(S, K, T, iv, r), Double.MIN_VALUE);
      error = bs.price(S, K, T, iv, r, call) - V;
    } while ((abs(error) > TARGET_MAX_ERROR) && (numIterations++ < MAX_ITERATIONS));
    if ((abs(error) > MAX_TOLERABLE_ERROR) || (abs(iv) >= 1000.0)) {
      return 0.0;
    }
    return iv;
  }

  public double impliedVolatility(Option option) {
    double V = option.mid();
    double S = option.underlying().mid();
    double K = option.strike();
    double T = option.yearsToExpiration();
    boolean call = option.optionType() == OptionType.CALL;
    return impliedVolatility(V, S, K, T, call);
  }

  /* THETA */
  public double theta(double V, double S, double K, double T, double r, boolean call) {
    if (T <= MINIMUM_EFFECTIVE_T) {
      return 0.0;
    }
    double iv = impliedVolatility(V, S, K, T, r, call);
    return bs.theta(S, K, T, iv, r, call);
  }

  public double theta(Option option) {
    double iv = impliedVolatility(option);
    double S = option.underlying().mid();
    double K = option.strike();
    double T = option.yearsToExpiration();
    boolean call = option.optionType() == OptionType.CALL;
    return bs.theta(S, K, T, iv, RISK_FREE_RATE, call);
  }
  
  public double vega(Option option) {
    double iv = impliedVolatility(option);
    double S = option.underlying().mid();
    double K = option.strike();
    double T = option.yearsToExpiration();
    return bs.vega(S, K, T, iv, RISK_FREE_RATE);
  }
}
