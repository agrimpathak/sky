package edu.berkeley.sky.position;

import static edu.berkeley.sky.util.Preconditions.checkArgument;
import static edu.berkeley.sky.util.Preconditions.checkNotNull;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import edu.berkeley.sky.commission.CommissionCalculator;
import edu.berkeley.sky.market.Security;
import edu.berkeley.sky.market.SecurityType;

/**
 * A Portfolio stores, manages, and tracks performance of one or more Positions.
 *
 * @author Agrim Pathak
 */
public class Portfolio {

  private final CommissionCalculator commCalc;
  private double commission;
  private double realizedPnl;
  private final Map<Security, Position> securityToPosition = new HashMap<Security, Position>();
  private final List<Security> stagedForRemoval = new ArrayList<Security>(4);
  private double unrealizedPnl;

  public Portfolio(CommissionCalculator commCalc) {
    this.commCalc = commCalc;
  }

  public void close(Security security) {
    checkNotNull(security);
    Position position = securityToPosition.get(security);
    if (position == null) {
      return;
    }
    order(security, -position.size());
  }

  /**
   * @return The commision
   */
  public double commission() {
    return commission;
  }

  /**
   * Close all positions at market.
   */
  public void liquidate() {
    for (Position position : securityToPosition.values()) {
      realizedPnl += position.close();
      if (position.security().type() == SecurityType.Option){
        stagedForRemoval.add(position.security());
      }
    }
  }

  /**
   * Create a new position or modify an existing one.
   *
   * @param security
   * @param size The change size in position.
   * @return The position.
   */
  public Position order(Security security, int size) {
    checkArgument((security != null));
    commission += commCalc.calculate(security, size);
    Position position = securityToPosition.get(security);
    if (position == null) {
      position = new Position(security);
      securityToPosition.put(security, position);
    }
    realizedPnl += position.changeSizeBy(size);
    if (position.isFlat() && (position.security().type() == SecurityType.Option)) {
      stagedForRemoval.add(position.security());
    }
    return position;
  }

  /**
   * @return All positions currently held in the portfolio.
   */
  public Collection<Position> positions() {
    return securityToPosition.values();
  }

  /**
   * @return The portfolio's realized PNL, i.e. PNL resulting in trading activity from positions
   *         that are now flat.
   */
  public double realizedPnl() {
    return realizedPnl;
  }

  /**
   * @return The sum of unrealized and realized PNL.
   */
  public double totalPnl() {
    return realizedPnl + unrealizedPnl;
  }

  /**
   * @return The portfolio's unrealized PNL, i.e. PNL of open positions.
   */
  public double unrealizedPnl() {
    return unrealizedPnl;
  }

  /**
   * Update unrealized PNL in order to reflect current, up-to-date security prices.
   */
  public void updateUnrealizedPnl() {
    unrealizedPnl = 0;
    for (Position position : securityToPosition.values()) {
      position.updateUnrealizedPnl();
      unrealizedPnl += position.unrealizedPnl();
    }
  }
  
  public void removeStaged(){
	for (Security sec : stagedForRemoval){
	  securityToPosition.remove(sec);
	}
	stagedForRemoval.clear();
  }
}
