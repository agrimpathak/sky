package edu.berkeley.sky.position;

import static edu.berkeley.sky.util.Preconditions.checkArgument;
import static edu.berkeley.sky.util.Preconditions.checkNotNull;

import java.util.List;

import org.joda.time.LocalDate;

import edu.berkeley.sky.market.Market;
import edu.berkeley.sky.market.OptionType;
import edu.berkeley.sky.market.Security;
import edu.berkeley.sky.util.OptionUtil;

public class SpreadBuilder implements Cloneable {

  private int expirationWeek0;
  private int expirationWeek1;
  private Market market;
  private OptionType optType0;
  private OptionType optType1;
  private int otmAmount0;
  private int otmAmount1;
  private int size0;
  private int size1;
  private Security underlying;

  public Position[] build() {
    validate();
    List<LocalDate> expirations = market.getExpirations(underlying.ticker());
    int atm = OptionUtil.atTheMoneyPrice(underlying);
    int sign0 = optType0 == OptionType.CALL ? 1 : -1;
    int sign1 = optType1 == OptionType.CALL ? 1 : -1;
    Security opt0 = market.requestOption(
    /**/underlying, expirations.get(expirationWeek0), atm + (sign0 * otmAmount0), optType0);
    Security opt1 = market.requestOption(
    /**/underlying, expirations.get(expirationWeek1), atm + (sign1 * otmAmount1), optType1);
    return new Position[] {new Position(opt0, size0), new Position(opt1, size1)};
  }

  @Override
  public SpreadBuilder clone() {
    return new SpreadBuilder()
    /**/.withMarket(market)
    /**/.withUnderlying(underlying)
    /**/.withExpirationWeek0(expirationWeek0)
    /**/.withExpirationWeek1(expirationWeek1)
    /**/.withOptionType0(optType0)
    /**/.withOptionType1(optType1)
    /**/.withOtmAmount0(otmAmount0)
    /**/.withOtmAmount1(otmAmount1)
    /**/.withSize0(size0)
    /**/.withSize1(size1);
  }

  private void validate() {
    checkNotNull(market, underlying, optType0, optType1);
    checkArgument(size0 != 0, size1 != 0);
  }

  public SpreadBuilder withExpirationWeek0(int expirationWeek0) {
    this.expirationWeek0 = expirationWeek0;
    return this;
  }

  public SpreadBuilder withExpirationWeek1(int expirationWeek1) {
    this.expirationWeek1 = expirationWeek1;
    return this;
  }

  public SpreadBuilder withMarket(Market market) {
    this.market = market;
    return this;
  }

  public SpreadBuilder withOptionType0(OptionType optType0) {
    this.optType0 = optType0;
    return this;
  }

  public SpreadBuilder withOptionType1(OptionType optType1) {
    this.optType1 = optType1;
    return this;
  }

  public SpreadBuilder withOtmAmount0(int otmAmount0) {
    this.otmAmount0 = otmAmount0;
    return this;
  }

  public SpreadBuilder withOtmAmount1(int otmAmount1) {
    this.otmAmount1 = otmAmount1;
    return this;
  }

  public SpreadBuilder withSize0(int size0) {
    this.size0 = size0;
    return this;
  }

  public SpreadBuilder withSize1(int size1) {
    this.size1 = size1;
    return this;
  }

  public SpreadBuilder withUnderlying(Security underlying) {
    this.underlying = underlying;
    return this;
  }
}
