package edu.berkeley.sky.position;

import static edu.berkeley.sky.util.Preconditions.checkNotNull;
import edu.berkeley.sky.market.Derivative;
import edu.berkeley.sky.market.Security;

/**
 * A Position mainly represents a security and its size.
 *
 * @author Agrim Pathak
 */
public class Position {

  private double realizedPnl;
  private final Security security;
  private int size;
  private final int sizeMultiplier;
  private double unrealizedPnl;
  private double wtdAvgPrice;

  Position(Security security) {
    checkNotNull(security);
    this.security = security;
    if (security instanceof Derivative) {
      sizeMultiplier = ((Derivative) security).contractSize();
    } else {
      sizeMultiplier = 1;
    }
  }

  Position(Security security, int initSize) {
    this(security);
    changeSizeBy(initSize);
  }

  /**
   * @return The price at which the position can be increased.
   */
  private double accumululationPrice(int change) {
    if (change > 0) {
      return security.ask();
    } else {
      return security.bid();
    }
  }

  /**
   * Change the position size.
   *
   * @param change
   * @param price
   * @return Change in realized PNL as a result of the position change.
   */
  double changeSizeBy(int change) {
    if (change == 0) {
      return 0.0;
      /*
       * If the change will create a position reversal, i.e. long to short or vice versa, then
       * record and return the realized pnl change and call changeBySize once more on the net amount
       * the position will be
       */
    } else if (isReversal(change)) {
      double pnlChange = (wtdAvgPrice - liquidationPrice()) * size * sizeMultiplier;
      realizedPnl += pnlChange;
      change += size;
      size = 0;
      changeSizeBy(change);
      return pnlChange;
      /*
       * If the size decreased, simply record the realized pnl change.
       */
    } else if (isSizeDecrease(change)) {
      double pnlChange = (wtdAvgPrice - liquidationPrice()) * change * sizeMultiplier;
      realizedPnl += pnlChange;
      size += change;
      if (size == 0) {
        wtdAvgPrice = 0;
      }
      return pnlChange;
      /*
       * else if size increased, simply update the weighted average price.
       */
    } else {
      wtdAvgPrice =
      /**/((size * wtdAvgPrice) + (change * accumululationPrice(change))) / (size + change);
      size += change;
      return 0.0;
    }
  }

  /**
   * @return Close the position and return the change in realized PNL.
   */
  double close() {
    return changeSizeBy(-size);
  }

  /**
   * @return A boolean indicating whether the position is flat, i.e. size == 0.
   */
  public boolean isFlat() {
    return size == 0;
  }

  /**
   * @return A boolean indicating whether the position is long, i.e. size > 0.
   */
  public boolean isLong() {
    return size > 0;
  }

  /**
   * @param change
   * @return A boolean indicating whether the size change will reverse the position.
   */
  private boolean isReversal(int change) {
    return ((size + change) * size) < 0;
  }

  /**
   * @return A boolean indicating whether the position is short, i.e. size < 0.
   */
  public boolean isShort() {
    return size < 0;
  }

  /**
   * @param change A change in position size
   * @return A boolean indicating whether the size change in decreasing the position size.
   */
  private boolean isSizeDecrease(int change) {
    return (change * size) < 0;
  }

  /**
   * @return The price at which the position can be reduced.
   */
  public double liquidationPrice() {
    if (isLong()) {
      return security.bid();
    } else if (isShort()) {
      return security.ask();
    } else {
      throw new IllegalArgumentException();
    }
  }

  /**
   * @return The position's realized PNL, i.e. PNL resulting in trading activity that is now closed.
   */
  public double realizedPnl() {
    return realizedPnl;
  }

  /**
   * @return The position's security.
   */
  public Security security() {
    return security;
  }

  /**
   * @return The size of the position, i.e. number of units (shares, contracts) bought of the
   *         security.
   */
  public int size() {
    return size;
  }

  /**
   * @return The position's unrealized PNL + realized PNL.
   */
  public double totalPnl() {
    return realizedPnl + unrealizedPnl;
  }

  /**
   * @return The position's unrealized PNL, i.e. PNL resulting in open trading activity
   */
  public double unrealizedPnl() {
    return unrealizedPnl;
  }

  /**
   * Update unrealized PNL in order to reflect current, up-to-date security prices.
   */
  void updateUnrealizedPnl() {
    if (isFlat()) {
      unrealizedPnl = 0;
    } else {
      unrealizedPnl = (liquidationPrice() - wtdAvgPrice) * size * sizeMultiplier;
    }
  }

  /**
   * @return The weighted average price of this position.
   */
  public double weightedAveragePrice() {
    return wtdAvgPrice;
  }
}
