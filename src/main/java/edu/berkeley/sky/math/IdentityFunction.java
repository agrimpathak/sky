package edu.berkeley.sky.math;

/**
 *
 * @author Agrim Pathak
 */
public class IdentityFunction implements Function {

  @Override
  public double eval(double arg) {
    return arg;
  }
}
