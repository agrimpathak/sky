package edu.berkeley.sky.math;

/**
 * A function takes in an input as argument, and returns an output
 *
 * @author Agrim Pathak
 */
public interface Function {

  double eval(double arg);
}
