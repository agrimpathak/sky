package edu.berkeley.sky.math;

/**
 * A linear transformation is a function of the form X -> mX + b
 *
 * @author Agrim Pathak
 *
 */
public class LinearTransform implements Function {

  private final double b;
  private final double m;

  public LinearTransform(double m, double b) {
    this.m = m;
    this.b = b;
  }

  @Override
  public double eval(double arg) {
    return (arg * m) + b;
  }
}
