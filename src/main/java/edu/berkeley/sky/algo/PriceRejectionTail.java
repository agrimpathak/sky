package edu.berkeley.sky.algo;

import static edu.berkeley.sky.util.Preconditions.checkArgument;

import java.util.ArrayDeque;
import java.util.Collections;
import java.util.Comparator;
import java.util.PriorityQueue;
import java.util.Queue;

import org.joda.time.DateTime;

import edu.berkeley.sky.algo.param.PriceRejectionTailParams;
import edu.berkeley.sky.algo.param.PriceRejectionTailParamsBuilder;
import edu.berkeley.sky.position.Position;

/**
 * @author Agrim Pathak
 */
public class PriceRejectionTail
    extends AbstractTradingAlgo<PriceRejectionTailParamsBuilder, PriceRejectionTailParams> {

  private class PriceTimestampPair {
    final double price;
    final DateTime timestamp;

    private PriceTimestampPair(DateTime timestamp, double price) {
      this.timestamp = timestamp;
      this.price = price;
    }
  }

  private static final Comparator<PriceTimestampPair> priceTimestampComp =
  /**/new Comparator<PriceTimestampPair>() {
    @Override
    public int compare(PriceTimestampPair o1, PriceTimestampPair o2) {
      return o1.price < o2.price ? -1 : 1;
    }
  };

  private double close;
  private final Queue<PriceTimestampPair> highs;
  private final Queue<PriceTimestampPair> lows;
  private int numMinutes;
  private final Queue<Double> opens;
  private final Position position;
  private double profitPrice;
  private double range;
  private double stopPrice;

  public PriceRejectionTail(PriceRejectionTailParams p) {
    super(p);
    position = p.portfolio().order(p.instrument(), 0);
    highs = new PriorityQueue<PriceTimestampPair>(2 * p.lookbackMinutes(),
    /**/Collections.reverseOrder(priceTimestampComp));
    lows = new PriorityQueue<PriceTimestampPair>(2 * p.lookbackMinutes(), priceTimestampComp);
    opens = new ArrayDeque<Double>(p.lookbackMinutes());
  }

  @Override
  protected void managePosition() {}

  @Override
  protected void openPosition() {
    if (close < highs.peek().price) {
      p.portfolio().order(p.instrument(), -1);
      profitPrice = close - (range * p.profitTargetFactorOfBarRange());
      stopPrice = close + (range * p.stopLossFactorOfBarRange());
    } else {
      p.portfolio().order(p.instrument(), 1);
      profitPrice = close + (range * p.profitTargetFactorOfBarRange());
      stopPrice = close - (range * p.stopLossFactorOfBarRange());
    }
  }

  @Override
  protected boolean triggerEntry() {
    if ((numMinutes < p.lookbackMinutes()) || (range < p.minPriceRange())) {
      return false;
    }
    double upperTail = highs.peek().price - Math.max(opens.peek(), close);
    double lowerTail = Math.min(opens.peek(), close) - lows.peek().price;
    return Math.max(upperTail, lowerTail) >= (range * p.tailRatio());
  }

  @Override
  protected boolean triggerExit() {
    if (super.triggerExit()) {
      return true;
    }
    if (position.isLong()) {
      return (p.instrument().high() >= profitPrice) || (p.instrument().low() <= stopPrice);
    } else {
      return (p.instrument().low() <= profitPrice) || (p.instrument().high() >= stopPrice);
    }
  }

  @Override
  protected void update() {
    // TODO: Handle case where first minute of tradeTimeInterva.startTime() is missing
    if (p.simulationDateTime().equals(p.tradeTimeInterval().startTime())) {
      numMinutes = 0;
      opens.clear();
      highs.clear();
      lows.clear();
    } else if (numMinutes > p.lookbackMinutes()) {
      opens.poll();
      DateTime boundaryTime = p.simulationDateTime().toDateTime().minusMinutes(p.lookbackMinutes());
      while (highs.peek().timestamp.isBefore(boundaryTime)) {
        highs.poll();
      }
      while (lows.peek().timestamp.isBefore(boundaryTime)) {
        lows.poll();
      }
      checkArgument(!highs.isEmpty(), !lows.isEmpty(), opens.size() == (-1 + p.lookbackMinutes()));
    }
    opens.add(p.instrument().open());
    DateTime dt = p.simulationDateTime().toDateTime();
    highs.add(new PriceTimestampPair(dt, p.instrument().high()));
    lows.add(new PriceTimestampPair(dt, p.instrument().low()));
    close = p.instrument().close();
    numMinutes++;
    range = highs.peek().price - lows.peek().price;
  }
}
