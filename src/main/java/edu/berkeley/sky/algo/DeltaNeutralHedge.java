package edu.berkeley.sky.algo;

import static java.lang.Math.abs;
import static java.lang.Math.round;
import edu.berkeley.sky.algo.param.DNHedgeParams;
import edu.berkeley.sky.algo.param.DNHedgeParamsBuilder;
import edu.berkeley.sky.analytics.PortfolioAnalytics;
import edu.berkeley.sky.market.Derivative;
import edu.berkeley.sky.position.Position;

/**
 * A delta-neutral strategy which neutralizes delta via stock.
 *
 * @author Agrim Pathak
 */
public class DeltaNeutralHedge extends AbstractTradingAlgo<DNHedgeParamsBuilder, DNHedgeParams> {

  private final double contractSize;

  public DeltaNeutralHedge(DNHedgeParams p) {
    super(p);
    if (p.hedgeInstrument() instanceof Derivative) {
      contractSize = ((Derivative) p.hedgeInstrument()).contractSize();
    } else {
      contractSize = 1.0;
    }
  }

  @Override
  protected void managePosition() {
    if (!triggerEntry()) {
      return;
    }
    int newSize = (int) round(-portOptionDelta() / contractSize);
    Integer size = securityToSize.get(p.hedgeInstrument());
    size = size == null ? 0 : size;
    int change = newSize - size;
    if ((abs(newSize) < p.minHedgeSize())) {
      p.portfolio().close(p.hedgeInstrument());
      securityToSize.remove(p.hedgeInstrument());
    } else if (abs(change) > p.maxUnhedgedDelta()) {
      p.portfolio().order(p.hedgeInstrument(), change);
      securityToSize.put(p.hedgeInstrument(), newSize);
    }
  }

  @Override
  protected void openPosition() {
    managePosition();
  }

  protected double portOptionDelta() {
    return p.deltaAdjustmentFunction().eval(
    /**/PortfolioAnalytics.optionDelta(p.portfolio(), p.deltaCalculator()));
  }

  @Override
  protected boolean triggerEntry() {
    return true;
  }

  @Override
  protected boolean triggerExit() {
    if (super.triggerExit()) {
      return true;
    }
    /* Exit if this is the only non-flat security */
    for (Position pos : p.portfolio().positions()){
    	if (!pos.security().equals(p.hedgeInstrument()) && !pos.isFlat()){
    		return false;
    	}
    }
    return true;
  }

  @Override
  protected void update() {}
}
