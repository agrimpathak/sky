package edu.berkeley.sky.algo;

/**
 * Interface to all trade algorithms
 *
 * @author Agrim Pathak
 */
public interface Algo {

  /**
   * Run the algorithm
   */
  void execute();
}
