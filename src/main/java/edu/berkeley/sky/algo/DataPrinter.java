package edu.berkeley.sky.algo;

import static edu.berkeley.sky.util.OptionUtil.atTheMoneyPrice;

import java.util.HashMap;
import java.util.Map;

import edu.berkeley.sky.algo.param.OptionDataPrinterParams;
import edu.berkeley.sky.market.OptionType;
import edu.berkeley.sky.market.Security;
import edu.berkeley.sky.report.data.DataRecord;

/**
 * Initializes the market for DataRecorders, DataExtractors and DataRecords
 *
 * @author Agrim Pathak
 */
public class DataPrinter implements Algo {

  private final Map<Double, Security> strikeToCall = new HashMap<Double, Security>();
  private final Map<Double, Security> strikeToPut = new HashMap<Double, Security>();

  public DataPrinter(OptionDataPrinterParams p) {
    double underlyingPx = atTheMoneyPrice(p.underlying());
    int capacity = 1 + (int) ((p.strikeUpperLimit() + p.strikeLowerLimit()) / p.strikeInterval());
    double lowerStrike = -p.strikeLowerLimit() + underlyingPx;
    double upperStrike = +p.strikeLowerLimit() + underlyingPx;
    double[] strikes = new double[capacity];
    int i = 0;
    for (double k = lowerStrike; k <= upperStrike; k += p.strikeInterval()) {
      strikeToCall.put(k,
      /**/p.market().requestOption(p.underlying(), p.expiration(), k, OptionType.CALL));
      strikeToPut.put(k,
      /**/p.market().requestOption(p.underlying(), p.expiration(), k, OptionType.PUT));
      strikes[i++] = k;
    }
    DataRecord.initHeader(p.market());
  }

  @Override
  public void execute() {}
}
