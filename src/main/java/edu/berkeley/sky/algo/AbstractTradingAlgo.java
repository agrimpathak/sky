package edu.berkeley.sky.algo;

import java.util.HashMap;
import java.util.Map;

import edu.berkeley.sky.algo.param.TradingAlgoParams;
import edu.berkeley.sky.algo.param.TradingAlgoParamsBuilder;
import edu.berkeley.sky.market.Security;

/**
 * Abstract strategy class.
 *
 * @author Agrim Pathak
 */
abstract class AbstractTradingAlgo<B extends TradingAlgoParamsBuilder<B>, P extends TradingAlgoParams<B>>
    implements
      Algo {

  protected final P p;
  /**
   * A map that separately maintains the algo's own holdings from other algos. This is because
   * various algos may interact with the same portfolio.
   *
   * This must not include any securities with size == 0, as it cause isFlat() to not work properly
   */
  protected final Map<Security, Integer> securityToSize = new HashMap<Security, Integer>(2);

  AbstractTradingAlgo(P p) {
    this.p = p;
  }

  /**
   * Close all positions executed by this algo. Securities must be removed by the market in derived
   * classes
   *
   * @param algoPositionSummary
   */
  protected void closeAllPositions() {
    for (Security security : securityToSize.keySet()) {
      p.portfolio().order(security, -securityToSize.get(security));
    }
    securityToSize.clear();
  }

  @Override
  public void execute() {
    /*
     * If simulation time is not within position time, ensure the position is flat.
     */
    if (!p.positionTimeInterval().contains(p.simulationDateTime())) {
      closeAllPositions();
      /*
       * else if simulation time is within trade time, then trade
       */
    } else if (p.tradeTimeInterval().contains(p.simulationDateTime())) {
      if (isFlat()) {
        if (triggerEntry()) {
          openPosition();
        }
      } else if (triggerExit()) {
        closeAllPositions();
      } else {
        managePosition();
      }
    }
  }

  /**
   * @return A boolean indicating that the algorithm is flat.
   */
  protected boolean isFlat() {
    return securityToSize.isEmpty();
  }

  /**
   * Manage and maintain a currently open position.
   */
  protected abstract void managePosition();

  /**
   * Open a new position.
   */
  protected abstract void openPosition();

  /**
   * @return A boolean indicating a new position can be opened.
   */
  protected abstract boolean triggerEntry();

  /**
   * @return A boolean indicating whether a current position must be closed.
   */
  protected boolean triggerExit() {
    for (Security sec : securityToSize.keySet()) {
      if (!sec.hasPriceData()) {
        return true;
      }
    }
    return false;
  }

  /**
   * Notify the strategy of a new incoming time (e.g. a new minute) and perform any preparations.
   */
  protected abstract void update();
}
