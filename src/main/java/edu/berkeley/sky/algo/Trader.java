package edu.berkeley.sky.algo;

import java.util.List;

/**
 * A composite class of algorithms.
 *
 * @author Agrim Pathak
 */
public class Trader {

  private final List<Algo> strategies;

  public Trader(List<Algo> strategies) {
    this.strategies = strategies;
  }

  /**
   * Execute each strategy in order as passed in the constructor of the algorithm.
   */
  public void trade() {
    for (Algo strategy : strategies) {
      strategy.execute();
    }
  }
}
