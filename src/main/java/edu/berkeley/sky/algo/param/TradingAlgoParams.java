package edu.berkeley.sky.algo.param;

import org.joda.time.MutableDateTime;

import edu.berkeley.sky.market.Market;
import edu.berkeley.sky.position.Portfolio;
import edu.berkeley.sky.util.TimeInterval;

/**
 * Strategy parameters.
 *
 * @author Agrim Pathak
 */
public class TradingAlgoParams<B extends TradingAlgoParamsBuilder<B>> {

  public static <P extends TradingAlgoParamsBuilder<P>> TradingAlgoParamsBuilder<P> builder() {
    return new TradingAlgoParamsBuilder<P>();
  }

  protected final B b;
  private final TimeInterval positionTimeInterval;
  private final TimeInterval tradeTimeInterval;

  protected TradingAlgoParams(B b) {
    this.b = b;
    positionTimeInterval = new TimeInterval(b.positionTimeInterval);
    tradeTimeInterval = new TimeInterval(b.tradeTimeInterval);
  }

  public Market market() {
    return b.market;
  }

  public Portfolio portfolio() {
    return b.portfolio;
  }

  public TimeInterval positionTimeInterval() {
    return positionTimeInterval;
  }

  public MutableDateTime simulationDateTime() {
    return b.simulationDateTime;
  }

  public TimeInterval tradeTimeInterval() {
    return tradeTimeInterval;
  }
}
