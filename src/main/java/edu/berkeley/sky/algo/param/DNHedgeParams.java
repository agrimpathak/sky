package edu.berkeley.sky.algo.param;

import edu.berkeley.sky.analytics.DeltaCalculator;
import edu.berkeley.sky.market.Security;
import edu.berkeley.sky.math.Function;

/**
 * Parameters specific to delta-neutral strategies.
 *
 * @author Agrim Pathak
 */
public class DNHedgeParams extends TradingAlgoParams<DNHedgeParamsBuilder> {

  @SuppressWarnings("unchecked")
  // TODO What is this warning?
  public static DNHedgeParamsBuilder builder() {
    return new DNHedgeParamsBuilder();
  }

  private final int maxUnhedgedDelta;
  private final int minHedgeSize;

  DNHedgeParams(DNHedgeParamsBuilder b) {
    super(b);
    maxUnhedgedDelta = b.maxUnhedgedDelta;
    minHedgeSize = b.minHedgeSize;
  }

  public Function deltaAdjustmentFunction() {
    return b.deltaAdjustmentFunction;
  }

  public DeltaCalculator deltaCalculator() {
    return b.deltaCalculator;
  }

  public Security hedgeInstrument() {
    return b.hedgeInstrument;
  }

  public int maxUnhedgedDelta() {
    return maxUnhedgedDelta;
  }

  public int minHedgeSize() {
    return minHedgeSize;
  }
}
