package edu.berkeley.sky.algo.param;

import static edu.berkeley.sky.util.Preconditions.checkNotNull;

import org.joda.time.MutableDateTime;

import edu.berkeley.sky.market.Market;
import edu.berkeley.sky.position.Portfolio;
import edu.berkeley.sky.util.TimeInterval;

/**
 * @author Agrim Pathak
 * @param <B>
 */
public class TradingAlgoParamsBuilder<B> {

  Market market;
  Portfolio portfolio;
  TimeInterval positionTimeInterval;
  MutableDateTime simulationDateTime;
  TimeInterval tradeTimeInterval;

  TradingAlgoParamsBuilder() {}

  public B cloneBase(TradingAlgoParams<? extends TradingAlgoParamsBuilder<?>> p) {
    this.market = p.market();
    this.portfolio = p.portfolio();
    this.positionTimeInterval = new TimeInterval(p.positionTimeInterval());
    this.simulationDateTime = p.simulationDateTime();
    this.tradeTimeInterval = new TimeInterval(p.tradeTimeInterval());
    return self();
  }

  /**
   * Copy contents of argument builder
   */
  public B cloneBase(TradingAlgoParamsBuilder<? extends TradingAlgoParamsBuilder<?>> b) {
    this.market = b.market;
    this.portfolio = b.portfolio;
    this.positionTimeInterval = new TimeInterval(b.positionTimeInterval);
    this.simulationDateTime = b.simulationDateTime;
    this.tradeTimeInterval = new TimeInterval(b.tradeTimeInterval);
    return self();
  }

  @SuppressWarnings("unchecked")
  protected B self() {
    return (B) this;
  }

  protected void validate() {
    checkNotNull(market, portfolio, simulationDateTime, tradeTimeInterval, positionTimeInterval);
  }

  /**
   * The market used by the simulation
   **/
  public B withMarket(Market market) {
    this.market = market;
    return self();
  }

  /**
   * The portfolio used by all strategies
   **/
  public B withPortfolio(Portfolio portfolio) {
    this.portfolio = portfolio;
    return self();
  }

  /**
   * The end time at which no positions can be held
   **/
  public B withPositionTimeInterval(TimeInterval positionTimeInterval) {
    this.positionTimeInterval = positionTimeInterval;
    return self();
  }

  /**
   * The simulation's time object
   **/
  public B withSimulationDateTime(MutableDateTime simulationDateTime) {
    this.simulationDateTime = simulationDateTime;
    return self();
  }

  /**
   * The start time at which strategies may commence trade
   **/
  public B withTradeTimeInterval(TimeInterval tradeTimeInterval) {
    this.tradeTimeInterval = tradeTimeInterval;
    return self();
  }
}
