package edu.berkeley.sky.algo.param;

import static edu.berkeley.sky.util.Preconditions.checkArgument;
import static edu.berkeley.sky.util.Preconditions.checkNotNull;

import org.joda.time.LocalDate;

import edu.berkeley.sky.market.Market;
import edu.berkeley.sky.market.Security;

/**
 * @author Agrim Pathak
 */
public class OptionDataPrinterParamsBuilder {

  LocalDate expiration;
  Market market;
  double strikeInterval;
  double strikeLowerLimit;
  double strikeUpperLimit;
  Security underlying;

  OptionDataPrinterParamsBuilder() {}

  public OptionDataPrinterParams build() {
    validate();
    return new OptionDataPrinterParams(this);
  }

  private void validate() {
    checkArgument(strikeInterval > 0, strikeLowerLimit > 0, strikeUpperLimit > 0);
    checkNotNull(market, underlying);
  }

  public OptionDataPrinterParamsBuilder withExpiration(LocalDate expiration) {
    this.expiration = expiration;
    return this;
  }

  public OptionDataPrinterParamsBuilder withMarket(Market market) {
    this.market = market;
    return this;
  }

  public OptionDataPrinterParamsBuilder withStrikeInterval(double strikeInterval) {
    this.strikeInterval = strikeInterval;
    return this;
  }

  public OptionDataPrinterParamsBuilder withStrikeLowerLimit(double strikeLowerLimit) {
    this.strikeLowerLimit = strikeLowerLimit;
    return this;
  }

  public OptionDataPrinterParamsBuilder withStrikeUpperLimit(double strikeUpperLimit) {
    this.strikeUpperLimit = strikeUpperLimit;
    return this;
  }

  public OptionDataPrinterParamsBuilder withUnderlying(Security underlying) {
    this.underlying = underlying;
    return this;
  }
}
