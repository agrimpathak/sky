package edu.berkeley.sky.algo.param;

import static edu.berkeley.sky.util.Preconditions.checkArgument;
import static edu.berkeley.sky.util.Preconditions.checkNotNull;
import edu.berkeley.sky.market.Security;

/**
 * @author Agrim Pathak
 */
public class PriceRejectionTailParamsBuilder
    extends TradingAlgoParamsBuilder<PriceRejectionTailParamsBuilder> {

  Security instrument;
  int lookbackMinutes;
  double minPriceRange;
  double profitTargetFactorOfBarRange;
  double stopLossFactorOfBarRange;
  double tailRatio;

  public PriceRejectionTailParams build() {
    validate();
    return new PriceRejectionTailParams(this);
  }

  @Override
  protected PriceRejectionTailParamsBuilder self() {
    return this;
  }

  @Override
  protected void validate() {
    super.validate();
    checkNotNull(instrument);
    checkArgument(minPriceRange > 0.0, profitTargetFactorOfBarRange > 0.0,
    /**/stopLossFactorOfBarRange > 0.0, tailRatio > 0.0);
  }

  public PriceRejectionTailParamsBuilder withInstrument(Security instrument) {
    this.instrument = instrument;
    return this;
  }

  public PriceRejectionTailParamsBuilder withLookBackMinutes(int lookbackMinutes) {
    this.lookbackMinutes = lookbackMinutes;
    return this;
  }

  public PriceRejectionTailParamsBuilder withMinPriceRange(double minPriceRange) {
    this.minPriceRange = minPriceRange;
    return this;
  }

  public PriceRejectionTailParamsBuilder withProfitTargetFactorOfBarRange(
      double profitTargetFactorOfBarRange) {
    this.profitTargetFactorOfBarRange = profitTargetFactorOfBarRange;
    return this;
  }

  public PriceRejectionTailParamsBuilder withStopLossFactorOfBarRange(
      double stopLossFactorOfBarRange) {
    this.stopLossFactorOfBarRange = stopLossFactorOfBarRange;
    return this;
  }

  public PriceRejectionTailParamsBuilder withTailRatio(double tailRatio) {
    this.tailRatio = tailRatio;
    return this;
  }
}
