package edu.berkeley.sky.algo.param;

import edu.berkeley.sky.market.Security;

/**
 * @author Agrim Pathak
 */
public class PriceRejectionTailParams extends TradingAlgoParams<PriceRejectionTailParamsBuilder> {

  // TODO: What is this warning?
  @SuppressWarnings("unchecked")
  public static PriceRejectionTailParamsBuilder builder() {
    return new PriceRejectionTailParamsBuilder();
  }

  private final int lookbackMinutes;
  private final double minPriceRange;
  private final double profitTargetFactorOfBarRange;
  private final double stopLossFactorOfBarRange;
  private final double tailRatio;

  protected PriceRejectionTailParams(PriceRejectionTailParamsBuilder b) {
    super(b);
    lookbackMinutes = b.lookbackMinutes;
    minPriceRange = b.minPriceRange;
    profitTargetFactorOfBarRange = b.profitTargetFactorOfBarRange;
    stopLossFactorOfBarRange = b.stopLossFactorOfBarRange;
    tailRatio = b.tailRatio;
  }

  public Security instrument() {
    return b.instrument;
  }

  public int lookbackMinutes() {
    return lookbackMinutes;
  }

  public double minPriceRange() {
    return minPriceRange;
  }

  public double profitTargetFactorOfBarRange() {
    return profitTargetFactorOfBarRange;
  }

  public double stopLossFactorOfBarRange() {
    return stopLossFactorOfBarRange;
  }

  public double tailRatio() {
    return tailRatio;
  }
}
