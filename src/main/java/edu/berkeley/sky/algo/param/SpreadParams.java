package edu.berkeley.sky.algo.param;

import edu.berkeley.sky.market.Security;
import edu.berkeley.sky.position.SpreadBuilder;

/**
 * @author Agrim Pathak
 */
public class SpreadParams extends TradingAlgoParams<SpreadParamsBuilder> {

  @SuppressWarnings("unchecked")
  // TODO What is this warning?
  public static SpreadParamsBuilder builder() {
    return new SpreadParamsBuilder();
  }

  private final double maxBidAskSpreadOnEntry;
  private final long millisBeforeExpiration;
  private final double minOtmAmountBeforeExit;
  private final double minPrice;

  SpreadParams(SpreadParamsBuilder b) {
    super(b);
    maxBidAskSpreadOnEntry = b.maxBidAskSpreadOnEntry;
    minOtmAmountBeforeExit = b.minOtmAmountBeforeExit;
    millisBeforeExpiration = b.millisBeforeExpiration;
    minPrice = b.minPrice;
  }

  public double maxBidAskSpreadOnEntry() {
    return maxBidAskSpreadOnEntry;
  }

  public long millisBeforeExpiration() {
    return millisBeforeExpiration;
  }

  public double minOtmAmountBeforeExit() {
    return minOtmAmountBeforeExit;
  }

  public double minPrice() {
    return minPrice;
  }

  public SpreadBuilder spreadBuilder() {
    return b.spreadBuilder;
  }

  public Security underlying() {
    return b.underlying;
  }
}
