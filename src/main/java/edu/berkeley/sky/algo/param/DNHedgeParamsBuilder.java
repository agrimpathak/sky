package edu.berkeley.sky.algo.param;

import static edu.berkeley.sky.util.Preconditions.checkArgument;
import static edu.berkeley.sky.util.Preconditions.checkNotNull;
import edu.berkeley.sky.analytics.DeltaCalculator;
import edu.berkeley.sky.market.Security;
import edu.berkeley.sky.math.Function;
import edu.berkeley.sky.math.IdentityFunction;

/**
 * @author Agrim Pathak
 */
public class DNHedgeParamsBuilder extends TradingAlgoParamsBuilder<DNHedgeParamsBuilder> {

  Function deltaAdjustmentFunction;
  DeltaCalculator deltaCalculator;
  Security hedgeInstrument;
  int maxUnhedgedDelta;
  int minHedgeSize;

  DNHedgeParamsBuilder() {}

  public DNHedgeParams build() {
    if (deltaAdjustmentFunction == null) {
      deltaAdjustmentFunction = new IdentityFunction();
    }
    validate();
    return new DNHedgeParams(this);
  }

  @Override
  protected DNHedgeParamsBuilder self() {
    return this;
  }

  @Override
  protected void validate() {
    super.validate();
    checkNotNull(deltaAdjustmentFunction, deltaCalculator, hedgeInstrument);
    checkArgument(maxUnhedgedDelta > 0);
  }

  /**
   * Adjustment factor that scales the position
   */
  public DNHedgeParamsBuilder withDeltaAdjustmentFunction(Function deltaAdjustmentFunction) {
    this.deltaAdjustmentFunction = deltaAdjustmentFunction;
    return this;
  }

  /**
   * Delta calculation methodology that should be used.
   */
  public DNHedgeParamsBuilder withDeltaCalculator(DeltaCalculator deltaCalculator) {
    this.deltaCalculator = deltaCalculator;
    return this;
  }

  /**
   * Instrument that should be used to hedge.
   */
  public DNHedgeParamsBuilder withHedgeInstrument(Security hedgeInstrument) {
    this.hedgeInstrument = hedgeInstrument;
    return this;
  }


  /**
   * The maximum delta in absolute value a portfolio can reach before delta is neutralized.
   */
  public DNHedgeParamsBuilder withMaxUnhedgedDelta(int maxUnhedgedDelta) {
    this.maxUnhedgedDelta = maxUnhedgedDelta;
    return this;
  }

  /**
   * The minimum position size a hedge will enter. This prevents odd lots of 4 shares (for example).
   */
  public DNHedgeParamsBuilder withMinimumHedgeSize(int minHedgeSize) {
    this.minHedgeSize = minHedgeSize;
    return this;
  }
}
