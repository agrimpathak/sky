package edu.berkeley.sky.algo.param;

import static edu.berkeley.sky.util.Preconditions.checkArgument;
import static edu.berkeley.sky.util.Preconditions.checkNotNull;
import edu.berkeley.sky.market.Security;
import edu.berkeley.sky.position.SpreadBuilder;

/**
 * @author Agrim Pathak
 */
public class SpreadParamsBuilder extends TradingAlgoParamsBuilder<SpreadParamsBuilder>
    implements
      Cloneable {

  double maxBidAskSpreadOnEntry;
  long millisBeforeExpiration;
  double minOtmAmountBeforeExit;
  double minPrice;
  SpreadBuilder spreadBuilder;
  Security underlying;

  SpreadParamsBuilder() {}

  public SpreadParams build() {
    validate();
    return new SpreadParams(this);
  }

  @Override
  public SpreadParamsBuilder clone() {
    return new SpreadParamsBuilder().cloneBase(this)
    /**/.withMaxBidAskSpreadOnEntry(maxBidAskSpreadOnEntry)
    /**/.withMillisBeforeExpiration(millisBeforeExpiration)
    /**/.withMinOtmAmountBeforeExit(minOtmAmountBeforeExit)
    /**/.withMinOtmAmountBeforeExit(minOtmAmountBeforeExit)
    /**/.withMinPrice(minPrice)
    /**/.withSpreadBuilder(spreadBuilder)
    /**/.withUnderlying(underlying);
  }

  @Override
  protected SpreadParamsBuilder self() {
    return this;
  }

  @Override
  protected void validate() {
    super.validate();
    checkNotNull(spreadBuilder, underlying);
    checkArgument(maxBidAskSpreadOnEntry > 0, millisBeforeExpiration >= 0, minPrice >= 0);
  }

  public SpreadParamsBuilder withMaxBidAskSpreadOnEntry(double maxBidAskSpreadOnEntry) {
    this.maxBidAskSpreadOnEntry = maxBidAskSpreadOnEntry;
    return this;
  }

  public SpreadParamsBuilder withMillisBeforeExpiration(long millisBeforeExpiration) {
    this.millisBeforeExpiration = millisBeforeExpiration;
    return this;
  }

  public SpreadParamsBuilder withMinOtmAmountBeforeExit(double minOtmAmountBeforeExit) {
    this.minOtmAmountBeforeExit = minOtmAmountBeforeExit;
    return this;
  }

  public SpreadParamsBuilder withMinPrice(double minPrice) {
    this.minPrice = minPrice;
    return this;
  }

  public SpreadParamsBuilder withSpreadBuilder(SpreadBuilder spreadBuilder) {
    this.spreadBuilder = spreadBuilder;
    return this;
  }

  public SpreadParamsBuilder withUnderlying(Security underlying) {
    this.underlying = underlying;
    return this;
  }
}
