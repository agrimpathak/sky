package edu.berkeley.sky.algo.param;

import org.joda.time.LocalDate;

import edu.berkeley.sky.market.Market;
import edu.berkeley.sky.market.Security;

/**
 * @author Agrim Pathak
 */
public class OptionDataPrinterParams {

  public static OptionDataPrinterParamsBuilder builder() {
    return new OptionDataPrinterParamsBuilder();
  }

  private final OptionDataPrinterParamsBuilder b;
  private final LocalDate expiration;
  private final double strikeInterval;
  private final double strikeLowerLimit;

  private final double strikeUpperLimit;

  OptionDataPrinterParams(OptionDataPrinterParamsBuilder b) {
    this.b = b;
    expiration = b.expiration;
    strikeInterval = b.strikeInterval;
    strikeLowerLimit = b.strikeLowerLimit;
    strikeUpperLimit = b.strikeUpperLimit;
  }

  public LocalDate expiration() {
    return expiration;
  }

  public Market market() {
    return b.market;
  }

  public double strikeInterval() {
    return strikeInterval;
  }

  public double strikeLowerLimit() {
    return strikeLowerLimit;
  }

  public double strikeUpperLimit() {
    return strikeUpperLimit;
  }

  public Security underlying() {
    return b.underlying;
  }
}
