package edu.berkeley.sky.algo;

import edu.berkeley.sky.algo.param.SpreadParams;
import edu.berkeley.sky.algo.param.SpreadParamsBuilder;
import edu.berkeley.sky.market.Option;
import edu.berkeley.sky.position.Position;

/**
 * @author Agrim Pathak
 */
public class SpreadAlgo extends AbstractTradingAlgo<SpreadParamsBuilder, SpreadParams> {

  private static final int MAX_STALENESS = 10;
  private Option opt0;
  private Option opt1;

  public SpreadAlgo(SpreadParams p) {
    super(p);
    p.portfolio().order(p.underlying(), 0); // report underlying price
  }

  @Override
  protected void closeAllPositions() {
    super.closeAllPositions();
    p.market().removeSecurity(opt0);
    p.market().removeSecurity(opt1);
    opt0 = null;
    opt1 = null;
  }

  @Override
  protected void managePosition() {}

  @Override
  protected void openPosition() {
    Position[] positions;
    positions = p.spreadBuilder().build();
    opt0 = (Option) positions[0].security();
    opt1 = (Option) positions[1].security();
    boolean o0 = opt0.hasPriceData()
    /**/&& (opt0.bidAskSpread() < p.maxBidAskSpreadOnEntry())
    /**/&& (opt0.minutesSinceTimestamp() <= MAX_STALENESS)
    /**/&& optionIsValid(opt0);
    boolean o1 = opt1.hasPriceData()
    /**/&& (opt1.bidAskSpread() < p.maxBidAskSpreadOnEntry())
    /**/&& (opt1.minutesSinceTimestamp() <= MAX_STALENESS)
    /**/&& optionIsValid(opt1);
    if (!o0 || !o1) {
      // Do not call market.remove(): The algo may attempt to open the same position at next minute
      return;
    }
    securityToSize.put(opt0, positions[0].size());
    securityToSize.put(opt1, positions[1].size());
    p.portfolio().order(opt0, positions[0].size());
    p.portfolio().order(opt1, positions[1].size());
  }

  private boolean optionIsValid(Option option) {
    return (option.mid() >= p.minPrice())
    /**/&& ((option.optionType().sign() * (option.strike() - p.underlying().mid()))
    /*      */>= p.minOtmAmountBeforeExit())
    /**/&& (option.millisToExpiration() >= p.millisBeforeExpiration());
  }

  @Override
  protected boolean triggerEntry() {
    return true;
  }

  @Override
  protected boolean triggerExit() {
    if ((opt0 == null) || (opt1 == null)) {
      return false;
    }
    return !optionIsValid(opt0) || !optionIsValid(opt1);
  }

  @Override
  protected void update() {}
}
