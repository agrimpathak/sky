package edu.berkeley.sky.market;

import static org.junit.Assert.assertTrue;

import java.util.Queue;

import org.joda.time.DateTime;
import org.joda.time.MutableDateTime;
import org.junit.Before;
import org.junit.Test;

public class AbstractSecurityTest {

  private static final int _TIME_BATCH = 5;
  private static final int SIMULATION_DURATION = 4 * _TIME_BATCH;
  private final DateTime INIT_DATE = new DateTime(2014, 7, 1, 0, 0, 0, 0);
  private final double[] prices = new double[1 + SIMULATION_DURATION];
  private MutableDateTime simDateTime;
  private Stock stock;

  @SuppressWarnings("null")
  @Before
  public void setup() {
    simDateTime = new MutableDateTime(INIT_DATE);
    stock = new Stock("", simDateTime);
    /*
     * Fill the security with fictional prices: 1.00, 1.01, 1.02 etc
     */
    DateTime dt = new DateTime(INIT_DATE).plusMinutes(_TIME_BATCH);
    Queue<PriceStreamElement> priceStream = null;
    // priceStream = stock.priceStream(); //TODO
    PriceStreamElement elem;
    for (int t = _TIME_BATCH; t <= SIMULATION_DURATION; t++) {
      prices[t] = 1.0 + (t / 100.0);
      elem = new PriceStreamElement(dt, 0, 0, 0, prices[t], 0, 0); // TODO
      priceStream.add(elem);
      dt = dt.plusMinutes(1);
    }
  }

  @SuppressWarnings("null")
  @Test
  public void testRefresh() {
    /*
     * Mimic simulation: Within the time loop, time is incremented first, then the market is
     * refreshed
     */
    int t; // minute of the simulation
    for (t = 1; t < _TIME_BATCH; t++) {
      simDateTime.addMinutes(1);
      stock.refresh();
      assertTrue(t == simDateTime.getMinuteOfDay());
      assertTrue(!stock.hasPriceData() && (stock.bid() == 0));
      assertTrue(stock.minutesSinceTimestamp() == Integer.MAX_VALUE);
    }
    t--; // t is incremented 1 too many times as part of the for loop above
    assertTrue((t == (-1 + _TIME_BATCH)) && (t == simDateTime.getMinuteOfDay()));
    /*
     * Confirm the security has price data and that it's correct.
     */
    while (t < (2 * _TIME_BATCH)) {
      t++;
      simDateTime.addMinutes(1);
      stock.refresh();
      assertTrue(t == simDateTime.getMinuteOfDay());
      assertTrue(stock.hasPriceData() && (stock.bid() == prices[t]));
      assertTrue(stock.timestamp().equals(simDateTime));
      assertTrue(stock.minutesSinceTimestamp() == 0);
    }
    assertTrue((t == (2 * _TIME_BATCH)) && (t == simDateTime.getMinuteOfDay()));
    /*
     * Mimic missing data by deleting some of the security's prices
     */
    DateTime dt = simDateTime.toDateTime();
    Queue<PriceStreamElement> priceStream = null; // TODO
    // priceStream = stock.priceStream();
    int t_lastData = t;
    DateTime dt_lastData = simDateTime.toDateTime();
    for (int i = 0; i < _TIME_BATCH; i++) {
      dt = dt.plusMinutes(1);
      priceStream.remove(dt);
    }
    while (t < (3 * _TIME_BATCH)) {
      t++;
      simDateTime.addMinutes(1);
      stock.refresh();
      assertTrue(t == simDateTime.getMinuteOfDay());
      assertTrue(stock.hasPriceData() && (stock.bid() != 0));
      assertTrue(stock.bid() == prices[t_lastData]);
      assertTrue(stock.timestamp().equals(dt_lastData));
      assertTrue(stock.minutesSinceTimestamp() == (t - t_lastData));
    }
  }
}
