package edu.berkeley.sky.market;

import org.joda.time.DateTimeConstants;
import org.joda.time.LocalDate;
import org.joda.time.MutableDateTime;
import org.junit.Before;
import org.junit.Test;

import static java.lang.Math.abs;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

public class OptionTest {

  private static final double ERROR_TOLERANCE = 1e-5;
  private Option option;
  private MutableDateTime simDateTime;

  @Before
  public void setup() {
    simDateTime = new MutableDateTime(2014, 7, 1, 0, 0, 0, 0);
    LocalDate expiration = new LocalDate(2014, 7, 2);
    option = new Option(simDateTime, new Stock("", simDateTime), expiration, 0, OptionType.CALL);
  }

  @Test
  public void testDaysToExpiration() {
    double t = 1.0;
    assertEquals(option.daysToExpiration(), t);

    simDateTime.addMinutes(1);
    t -= 1.0 / DateTimeConstants.MINUTES_PER_DAY;
    assertTrue(abs(option.daysToExpiration() - t) < ERROR_TOLERANCE);

    simDateTime.addMinutes(1);
    t -= 1.0 / DateTimeConstants.MINUTES_PER_DAY;
    assertTrue(abs(option.daysToExpiration() - t) < ERROR_TOLERANCE);
  }

  @Test
  public void testYearsToExpiration() {
    double t = 1.0 / 365.0;
    assertTrue(abs(option.yearsToExpiration() - t) < ERROR_TOLERANCE);


    simDateTime.addMinutes(1);
    t -= 1.0 / (DateTimeConstants.MINUTES_PER_DAY * 365.0);
    assertTrue(abs(option.yearsToExpiration() - t) < ERROR_TOLERANCE);

    simDateTime.addMinutes(1);
    t -= 1.0 / (DateTimeConstants.MINUTES_PER_DAY * 365.0);
    assertTrue(abs(option.yearsToExpiration() - t) < ERROR_TOLERANCE);
  }
}
