package edu.berkeley.sky.analytics;

import org.junit.Test;

import static java.lang.Math.abs;
import static org.junit.Assert.assertTrue;

public class TestBlackScholes {

  private final static double ERROR_THRESHOLD = 1E-2;
  private final BlackScholes bs = new BlackScholes();
  private final double K = 155.0;
  private final double r = 0.01;
  private final double S = 151.22;
  private final double SIGMA = 0.12;
  private final double T = 0.20;

  @Test
  public void deltaTest() {
    double callDeltaexpected = 0.346;
    assertTrue(abs(bs.delta(S, K, T, SIGMA, r, true) - callDeltaexpected) < ERROR_THRESHOLD);
    double putDeltaexpected = -0.654;
    assertTrue(abs(bs.delta(S, K, T, SIGMA, r, false) - putDeltaexpected) < ERROR_THRESHOLD);
  }

  @Test
  public void gammaTest() {
    double gammaExpected = 0.045;
    assertTrue(abs(bs.gamma(S, K, T, SIGMA, r) - gammaExpected) < ERROR_THRESHOLD);
  }

  @Test
  public void priceTest() {
    double callPriceExpected = 1.827;
    assertTrue(abs(bs.price(S, K, T, SIGMA, r, true) - callPriceExpected) < ERROR_THRESHOLD);
    double putPriceExpected = 5.298;
    assertTrue(abs(bs.price(S, K, T, SIGMA, r, false) - putPriceExpected) < ERROR_THRESHOLD);
  }

  @Test
  public void rhoTest() {
    double callRhoExpected = 10.101;
    assertTrue(abs(bs.rho(S, K, T, SIGMA, r, true) - callRhoExpected) < ERROR_THRESHOLD);
    double putRhoExpected = -20.83;
    assertTrue(abs(bs.rho(S, K, T, SIGMA, r, false) - putRhoExpected) < ERROR_THRESHOLD);
  }

  @Test
  public void thetaTest() {
    double callThetaExpected = -7.989;
    assertTrue(abs(bs.theta(S, K, T, SIGMA, r, true) - callThetaExpected) < ERROR_THRESHOLD);
    double putThetaExpected = -6.442;
    assertTrue(abs(bs.theta(S, K, T, SIGMA, r, false) - putThetaExpected) < ERROR_THRESHOLD);
  }

  @Test
  public void vegaTest() {
    double vegaExpected = 24.945;
    assertTrue(abs(bs.vega(S, K, T, SIGMA, r) - vegaExpected) < ERROR_THRESHOLD);
  }
}
