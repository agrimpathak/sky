package edu.berkeley.sky.analytics;

import org.joda.time.DateTimeConstants;
import org.junit.Test;

import static java.lang.Math.abs;
import static org.junit.Assert.assertTrue;

public class TestBlackScholesImpliedVolatility {

  private final static double ERROR_THRESHOLD = 1E-3;
  private final BlackScholes bs = new BlackScholes();
  private final BlackScholesImpliedVolatility bsiv = new BlackScholesImpliedVolatility();
  private final double K = 195;
  private final double r = 0.01;
  private final double S = 194.77;
  private final double SIGMA = 0.12;

  @Test
  public void test1() {
    double T = 0.20;
    double V = bs.price(S, K, T, SIGMA, r, true);
    double iv = bsiv.impliedVolatility(V, S, K, T, r, true);
    assertTrue(abs(SIGMA - iv) < ERROR_THRESHOLD);
  }

  /**
   * Test at extreme (low) values of time
   */
  @Test
  public void test2() {
    final int MINUTES_PER_YEAR = 52 * DateTimeConstants.MINUTES_PER_WEEK;
    for (double sigma = 0.06; sigma < 0.25; sigma += 0.02) {
      double T, V, iv;
      int minuteCount = 2;
      while (minuteCount-- > 1) {
        T = minuteCount * (1.0 / MINUTES_PER_YEAR);
        V = bs.price(S, K, T, sigma, r, true);
        iv = bsiv.impliedVolatility(V, S, K, T, r, true);
        System.out.println("SIGMA: " + sigma + ", MINUTES: " + minuteCount + ", ERROR: "
            + abs(sigma - iv));
        // assertTrue(abs(sigma - iv) < ERROR_THRESHOLD);
      }
    }
  }
}
